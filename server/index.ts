import express from 'express';
import * as dotenv from "dotenv";
import { postgraphile } from 'postgraphile';
import PgSimplifyInflectorPlugin from '@graphile-contrib/pg-simplify-inflector';
import PgManyToManyPlugin from '@graphile-contrib/pg-many-to-many';
import ConnectionFilterPlugin from 'postgraphile-plugin-connection-filter';
import pg from 'pg';

dotenv.config();


const pgPool = new pg.Pool({
    connectionString: process.env.DATABASE_URL
});

const app_root_url = `${process.env.HTTP_OR_HTTPS}://${process.env.HOST}:${process.env.PORT}`;

const postgraphileOptions = {
    subscriptions: true,
    live: true,
    graphileBuildOptions: { pgSkipInstallingWatchFixtures: true },
    dynamicJson: true,
    setofFunctionsContainNulls: false,
    ownerConnectionString: process.env.DATABASE_URL,
    ignoreRBAC: false,
    ignoreIndexes: false,
    showErrorStack: 'json' as 'json',
    extendedErrors: ["hint", "detail", "errcode"],
    appendPlugins: [PgSimplifyInflectorPlugin, PgManyToManyPlugin, ConnectionFilterPlugin],
    exportGqlSchemaPath: "schema.graphql",
    graphiql: true,
    enhanceGraphiql: !(process.env.PRODUCTION && process.env.PRODUCTION === 'true'),
    allowExplain(req) {
        return true;
    },
    jwtSecret: process.env.JWT_SECRET,
    jwtPgTypeIdentifier: 'app_public.jwt_token',
    pgDefaultRole: 'friensco_visitor',
    enableQueryBatching: true,
    legacyRelations: 'omit' as 'omit',
};

const app = express();

app.use(
    postgraphile(
        pgPool,
        "app_public",
        postgraphileOptions,
    )
);

app.listen({ port: process.env.PORT }, () =>
    console.log(`🚀 Server ready at ${app_root_url}/graphql`)
);

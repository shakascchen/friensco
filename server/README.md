
# Installation
## Postgresql Order
### Superuser
```
CREATE USER mutualhelp_local password 'password'
```

and run ```db/superuser/role.sql``` and ```db/superuser/extension.sql```
### Application User

1. schema.sql
1. type.sql
1. table.sql
1. index.sql
1. functions_and_triggers.sql
1. role.sql

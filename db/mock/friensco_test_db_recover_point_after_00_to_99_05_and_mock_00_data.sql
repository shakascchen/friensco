--
-- PostgreSQL database dump
--

-- Dumped from database version 12.5
-- Dumped by pg_dump version 12.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE friensco_test_db;
--
-- Name: friensco_test_db; Type: DATABASE; Schema: -; Owner: friensco
--

CREATE DATABASE friensco_test_db WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';


ALTER DATABASE friensco_test_db OWNER TO friensco;

\connect friensco_test_db

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: app_hidden; Type: SCHEMA; Schema: -; Owner: friensco
--

CREATE SCHEMA app_hidden;


ALTER SCHEMA app_hidden OWNER TO friensco;

--
-- Name: app_private; Type: SCHEMA; Schema: -; Owner: friensco
--

CREATE SCHEMA app_private;


ALTER SCHEMA app_private OWNER TO friensco;

--
-- Name: app_public; Type: SCHEMA; Schema: -; Owner: friensco
--

CREATE SCHEMA app_public;


ALTER SCHEMA app_public OWNER TO friensco;

--
-- Name: hstore; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA public;


--
-- Name: EXTENSION hstore; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION hstore IS 'data type for storing sets of (key, value) pairs';


--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


--
-- Name: address_type; Type: TYPE; Schema: app_public; Owner: friensco
--

CREATE TYPE app_public.address_type AS ENUM (
    'cellphone',
    'fax',
    'fixedline_telephone',
    'email',
    'mail',
    'real_estate',
    'others'
);


ALTER TYPE app_public.address_type OWNER TO friensco;

--
-- Name: help_attitude; Type: TYPE; Schema: app_public; Owner: friensco
--

CREATE TYPE app_public.help_attitude AS ENUM (
    'looking_for_people_who',
    'is_a_person_who'
);


ALTER TYPE app_public.help_attitude OWNER TO friensco;

--
-- Name: jwt_token; Type: TYPE; Schema: app_public; Owner: friensco
--

CREATE TYPE app_public.jwt_token AS (
	role text,
	expire integer,
	user_id integer,
	username character varying
);


ALTER TYPE app_public.jwt_token OWNER TO friensco;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: users; Type: TABLE; Schema: app_public; Owner: friensco
--

CREATE TABLE app_public.users (
    id integer NOT NULL,
    name character varying NOT NULL,
    email character varying NOT NULL,
    avatar_url character varying NOT NULL,
    cellphone character varying,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    is_verified boolean DEFAULT false NOT NULL,
    who_you_are text NOT NULL
);


ALTER TABLE app_public.users OWNER TO friensco;

--
-- Name: manually_set_verified(integer); Type: FUNCTION; Schema: app_hidden; Owner: friensco
--

CREATE FUNCTION app_hidden.manually_set_verified(user_id integer) RETURNS app_public.users
    LANGUAGE sql STRICT
    AS $$
BEGIN;

UPDATE
  app_public.users
SET
  is_verified = TRUE
WHERE
  app_public.users.ID = user_id;

INSERT INTO app_public.friendship (source_id, target_id)
  VALUES (1, user_id), (user_id, 1);

COMMIT;

SELECT
  *
FROM
  app_public.users
WHERE
  app_public.users.ID = user_id
$$;


ALTER FUNCTION app_hidden.manually_set_verified(user_id integer) OWNER TO friensco;

--
-- Name: authenticate(text, text); Type: FUNCTION; Schema: app_public; Owner: friensco
--

CREATE FUNCTION app_public.authenticate(email text, password text) RETURNS app_public.jwt_token
    LANGUAGE plpgsql STRICT SECURITY DEFINER
    AS $$
DECLARE
  user_password_salted TEXT;
  user_email VARCHAR;
  user_name VARCHAR;
  user_id INT;
BEGIN
  SELECT
    app_public.users.ID,
    app_public.users.email,
    app_public.users.NAME,
    app_private.users.password_salted INTO user_id,
    user_email,
    user_name,
    user_password_salted
  FROM
    app_private.users
    INNER JOIN app_public.users ON app_private.users.ID = app_public.users.ID
  WHERE
    app_public.users.email = authenticate.email;
  IF user_password_salted = crypt(PASSWORD, user_password_salted) THEN
    RETURN ('friensco_user',
      extract(epoch FROM now() + interval '7 days'),
      user_id,
      user_name)::app_public.jwt_token;
  ELSE
    RETURN NULL;
  END IF;
END;
$$;


ALTER FUNCTION app_public.authenticate(email text, password text) OWNER TO friensco;

--
-- Name: get_current_user_id(); Type: FUNCTION; Schema: app_public; Owner: friensco
--

CREATE FUNCTION app_public.get_current_user_id() RETURNS integer
    LANGUAGE sql STABLE
    AS $$
  SELECT
    current_setting('jwt.claims.user_id', TRUE)::int;

$$;


ALTER FUNCTION app_public.get_current_user_id() OWNER TO friensco;

--
-- Name: help; Type: TABLE; Schema: app_public; Owner: friensco
--

CREATE TABLE app_public.help (
    id integer NOT NULL,
    user_id integer NOT NULL,
    help_attitude app_public.help_attitude NOT NULL,
    object_of_help_id integer NOT NULL,
    property_id integer NOT NULL,
    is_closed boolean DEFAULT false NOT NULL,
    searchable boolean DEFAULT true NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE app_public.help OWNER TO friensco;

--
-- Name: help_is_private_note(app_public.help); Type: FUNCTION; Schema: app_public; Owner: friensco
--

CREATE FUNCTION app_public.help_is_private_note(help_record app_public.help) RETURNS boolean
    LANGUAGE sql STABLE
    AS $$
  SELECT
    (0 < (
        SELECT
          COUNT(*)
        FROM
          app_public.help_group
        WHERE
          help_record.ID = app_public.help_group.help_id))
$$;


ALTER FUNCTION app_public.help_is_private_note(help_record app_public.help) OWNER TO friensco;

--
-- Name: signup(character varying, character varying, text, text); Type: FUNCTION; Schema: app_public; Owner: friensco
--

CREATE FUNCTION app_public.signup(email character varying, name character varying, password text, who_you_are text) RETURNS boolean
    LANGUAGE plpgsql STRICT SECURITY DEFINER
    AS $$
DECLARE
  user_password_salted TEXT = crypt(PASSWORD, gen_salt('bf'));
  user_email VARCHAR;
  user_name VARCHAR;
  user_id INT;
BEGIN
  WITH new_user AS (
INSERT INTO app_public.users (email, NAME, avatar_url, who_you_are)
      VALUES (email, NAME, 'https://robohash.org/' || SUBSTRING(md5(now()::TEXT), 1, 8), who_you_are)
    RETURNING
      app_public.users.ID, app_public.users.NAME, app_public.users.email)
    SELECT
      new_user.ID,
      new_user.NAME,
      new_user.email INTO user_id,
      user_name,
      user_email
    FROM
      new_user;
  -- TODO refine transaction management
  INSERT INTO app_private.users (ID, password_salted)
    VALUES (user_id, user_password_salted);
  -- TODO refine transaction management
  IF user_email IS NOT NULL AND user_name IS NOT NULL AND user_id IS NOT NULL THEN
    RETURN TRUE;
  ELSE
    RETURN FALSE;
  END IF;
  RETURN FALSE;
END;
$$;


ALTER FUNCTION app_public.signup(email character varying, name character varying, password text, who_you_are text) OWNER TO friensco;

--
-- Name: virtual_user; Type: TABLE; Schema: app_public; Owner: friensco
--

CREATE TABLE app_public.virtual_user (
    id integer NOT NULL,
    user_id integer NOT NULL,
    target_user_id integer,
    name character varying NOT NULL,
    display_name character varying NOT NULL,
    can_do_referral boolean DEFAULT false NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE app_public.virtual_user OWNER TO friensco;

--
-- Name: virtual_users_who_friends_can_do_referral_to_current_user(); Type: FUNCTION; Schema: app_public; Owner: friensco
--

CREATE FUNCTION app_public.virtual_users_who_friends_can_do_referral_to_current_user() RETURNS SETOF app_public.virtual_user
    LANGUAGE plpgsql STABLE
    AS $$
DECLARE
  current_user_id INT := current_setting('jwt.claims.user_id', TRUE)::INT;
BEGIN
  RETURN query
  SELECT
    *
  FROM
    app_public.virtual_user
  WHERE
    app_public.virtual_user.user_id IN (
      SELECT
        app_public.friendship.target_id
      FROM
        app_public.friendship
      WHERE
        app_public.friendship.source_id = current_user_id)
    AND app_public.virtual_user.can_do_referral = TRUE;
END;
$$;


ALTER FUNCTION app_public.virtual_users_who_friends_can_do_referral_to_current_user() OWNER TO friensco;

--
-- Name: users; Type: TABLE; Schema: app_private; Owner: friensco
--

CREATE TABLE app_private.users (
    id integer NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    password_salted text NOT NULL
);


ALTER TABLE app_private.users OWNER TO friensco;

--
-- Name: contact_address; Type: TABLE; Schema: app_public; Owner: friensco
--

CREATE TABLE app_public.contact_address (
    type app_public.address_type NOT NULL,
    usage character varying,
    address character varying NOT NULL,
    user_id integer NOT NULL,
    target_user_id integer NOT NULL,
    is_verified boolean DEFAULT false NOT NULL,
    is_user_self_info boolean GENERATED ALWAYS AS ((user_id = target_user_id)) STORED
);


ALTER TABLE app_public.contact_address OWNER TO friensco;

--
-- Name: friendship; Type: TABLE; Schema: app_public; Owner: friensco
--

CREATE TABLE app_public.friendship (
    source_id integer NOT NULL,
    target_id integer NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT friendship_check CHECK ((source_id <> target_id))
);


ALTER TABLE app_public.friendship OWNER TO friensco;

--
-- Name: help_group; Type: TABLE; Schema: app_public; Owner: friensco
--

CREATE TABLE app_public.help_group (
    help_id integer NOT NULL,
    virtual_user_id integer NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE app_public.help_group OWNER TO friensco;

--
-- Name: help_id_seq; Type: SEQUENCE; Schema: app_public; Owner: friensco
--

ALTER TABLE app_public.help ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME app_public.help_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: lazy_question; Type: TABLE; Schema: app_public; Owner: friensco
--

CREATE TABLE app_public.lazy_question (
    id integer NOT NULL,
    user_id integer NOT NULL,
    target_user_id integer,
    question text NOT NULL,
    is_closed boolean DEFAULT false NOT NULL,
    searchable boolean DEFAULT true NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    is_private_note boolean GENERATED ALWAYS AS ((target_user_id IS NOT NULL)) STORED
);


ALTER TABLE app_public.lazy_question OWNER TO friensco;

--
-- Name: lazy_question_id_seq; Type: SEQUENCE; Schema: app_public; Owner: friensco
--

ALTER TABLE app_public.lazy_question ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME app_public.lazy_question_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: object_of_help; Type: TABLE; Schema: app_public; Owner: friensco
--

CREATE TABLE app_public.object_of_help (
    id integer NOT NULL,
    name character varying NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE app_public.object_of_help OWNER TO friensco;

--
-- Name: object_of_help_id_seq; Type: SEQUENCE; Schema: app_public; Owner: friensco
--

ALTER TABLE app_public.object_of_help ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME app_public.object_of_help_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: property_node; Type: TABLE; Schema: app_public; Owner: friensco
--

CREATE TABLE app_public.property_node (
    id integer NOT NULL,
    name character varying NOT NULL,
    types character varying[] DEFAULT ARRAY[]::character varying[] NOT NULL,
    urls character varying DEFAULT ARRAY[]::character varying[] NOT NULL,
    description text DEFAULT ''::text NOT NULL,
    is_private boolean DEFAULT false NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE app_public.property_node OWNER TO friensco;

--
-- Name: property_node_id_seq; Type: SEQUENCE; Schema: app_public; Owner: friensco
--

ALTER TABLE app_public.property_node ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME app_public.property_node_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: app_public; Owner: friensco
--

ALTER TABLE app_public.users ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME app_public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: virtual_user_id_seq; Type: SEQUENCE; Schema: app_public; Owner: friensco
--

ALTER TABLE app_public.virtual_user ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME app_public.virtual_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Data for Name: users; Type: TABLE DATA; Schema: app_private; Owner: friensco
--

COPY app_private.users (id, updated_at, password_salted) FROM stdin;
1	2020-12-08 22:37:56.803536+08	$2a$08$PQLZW88x2lsugsZ3Nr5LTeIu0LLvPwRqQ1Uja8OgeTBUnGpH.1F0m
\.


--
-- Data for Name: contact_address; Type: TABLE DATA; Schema: app_public; Owner: friensco
--

COPY app_public.contact_address (type, usage, address, user_id, target_user_id, is_verified) FROM stdin;
\.


--
-- Data for Name: friendship; Type: TABLE DATA; Schema: app_public; Owner: friensco
--

COPY app_public.friendship (source_id, target_id, created_at) FROM stdin;
1	2	2020-12-08 22:37:56.803536+08
1	3	2020-12-08 22:37:56.803536+08
1	4	2020-12-08 22:37:56.803536+08
1	5	2020-12-08 22:37:56.803536+08
1	6	2020-12-08 22:37:56.803536+08
1	7	2020-12-08 22:37:56.803536+08
1	8	2020-12-08 22:37:56.803536+08
1	9	2020-12-08 22:37:56.803536+08
2	1	2020-12-08 22:37:56.803536+08
3	1	2020-12-08 22:37:56.803536+08
4	1	2020-12-08 22:37:56.803536+08
5	1	2020-12-08 22:37:56.803536+08
6	1	2020-12-08 22:37:56.803536+08
7	1	2020-12-08 22:37:56.803536+08
8	1	2020-12-08 22:37:56.803536+08
9	1	2020-12-08 22:37:56.803536+08
5	10	2020-12-08 22:37:56.803536+08
5	12	2020-12-08 22:37:56.803536+08
8	11	2020-12-08 22:37:56.803536+08
\.


--
-- Data for Name: help; Type: TABLE DATA; Schema: app_public; Owner: friensco
--

COPY app_public.help (id, user_id, help_attitude, object_of_help_id, property_id, is_closed, searchable, created_at) FROM stdin;
1	2	is_a_person_who	2	8	f	t	2020-12-08 22:37:56.803536+08
2	2	is_a_person_who	1	56	f	t	2020-12-08 22:37:56.803536+08
3	2	is_a_person_who	1	66	f	t	2020-12-08 22:37:56.803536+08
4	2	is_a_person_who	1	68	f	t	2020-12-08 22:37:56.803536+08
5	2	is_a_person_who	1	71	f	t	2020-12-08 22:37:56.803536+08
6	2	is_a_person_who	5	30	f	t	2020-12-08 22:37:56.803536+08
7	2	is_a_person_who	3	14	f	t	2020-12-08 22:37:56.803536+08
8	2	is_a_person_who	5	39	f	t	2020-12-08 22:37:56.803536+08
9	4	is_a_person_who	5	27	f	t	2020-12-08 22:37:56.803536+08
10	4	is_a_person_who	1	64	f	t	2020-12-08 22:37:56.803536+08
11	4	is_a_person_who	1	42	f	t	2020-12-08 22:37:56.803536+08
12	4	is_a_person_who	5	38	f	t	2020-12-08 22:37:56.803536+08
13	4	looking_for_people_who	6	84	f	t	2020-12-08 22:37:56.803536+08
14	5	is_a_person_who	2	9	f	t	2020-12-08 22:37:56.803536+08
15	5	is_a_person_who	1	45	f	t	2020-12-08 22:37:56.803536+08
16	5	is_a_person_who	6	83	f	t	2020-12-08 22:37:56.803536+08
17	8	is_a_person_who	3	18	f	t	2020-12-08 22:37:56.803536+08
18	8	is_a_person_who	3	13	f	t	2020-12-08 22:37:56.803536+08
19	8	is_a_person_who	1	23	f	t	2020-12-08 22:37:56.803536+08
20	8	is_a_person_who	1	49	f	t	2020-12-08 22:37:56.803536+08
21	8	is_a_person_who	1	60	f	t	2020-12-08 22:37:56.803536+08
22	8	is_a_person_who	1	72	f	t	2020-12-08 22:37:56.803536+08
23	8	is_a_person_who	1	62	f	t	2020-12-08 22:37:56.803536+08
24	1	is_a_person_who	2	11	f	t	2020-12-08 22:37:56.803536+08
25	1	is_a_person_who	2	12	f	t	2020-12-08 22:37:56.803536+08
26	1	is_a_person_who	5	32	f	t	2020-12-08 22:37:56.803536+08
27	6	is_a_person_who	5	28	f	t	2020-12-08 22:37:56.803536+08
28	6	is_a_person_who	1	64	f	t	2020-12-08 22:37:56.803536+08
29	6	is_a_person_who	1	21	f	t	2020-12-08 22:37:56.803536+08
30	6	is_a_person_who	1	59	f	t	2020-12-08 22:37:56.803536+08
31	6	is_a_person_who	1	71	f	t	2020-12-08 22:37:56.803536+08
32	6	is_a_person_who	1	82	f	t	2020-12-08 22:37:56.803536+08
33	6	is_a_person_who	4	15	f	t	2020-12-08 22:37:56.803536+08
34	9	is_a_person_who	5	26	f	t	2020-12-08 22:37:56.803536+08
35	9	is_a_person_who	1	43	f	t	2020-12-08 22:37:56.803536+08
36	9	is_a_person_who	5	33	f	t	2020-12-08 22:37:56.803536+08
37	9	looking_for_people_who	3	16	f	t	2020-12-08 22:37:56.803536+08
38	9	looking_for_people_who	1	79	f	t	2020-12-08 22:37:56.803536+08
39	1	looking_for_people_who	4	6	f	t	2020-12-11 23:57:46.637375+08
40	1	looking_for_people_who	1	22	f	t	2020-12-11 23:57:46.637375+08
41	1	is_a_person_who	1	80	f	t	2020-12-11 23:57:46.637375+08
42	1	looking_for_people_who	1	24	f	t	2020-12-11 23:57:46.637375+08
43	1	is_a_person_who	2	33	f	t	2020-12-11 23:57:46.643973+08
44	1	looking_for_people_who	3	27	f	t	2020-12-11 23:57:46.643973+08
45	1	is_a_person_who	1	70	f	t	2020-12-11 23:57:46.643973+08
46	1	looking_for_people_who	4	50	f	t	2020-12-11 23:57:46.643973+08
47	5	is_a_person_who	5	5	f	t	2020-12-11 23:57:46.643973+08
48	6	looking_for_people_who	6	10	f	t	2020-12-11 23:57:46.643973+08
49	7	is_a_person_who	1	23	f	t	2020-12-11 23:57:46.643973+08
50	7	looking_for_people_who	2	15	f	t	2020-12-11 23:57:46.643973+08
51	7	is_a_person_who	3	2	f	t	2020-12-11 23:57:46.643973+08
52	8	looking_for_people_who	4	38	f	t	2020-12-11 23:57:46.643973+08
53	9	is_a_person_who	5	19	f	t	2020-12-11 23:57:46.643973+08
54	12	looking_for_people_who	6	62	f	t	2020-12-11 23:57:46.643973+08
55	12	is_a_person_who	1	80	f	t	2020-12-11 23:57:46.643973+08
56	12	looking_for_people_who	2	12	f	t	2020-12-11 23:57:46.643973+08
57	12	is_a_person_who	3	4	f	t	2020-12-11 23:57:46.643973+08
58	14	looking_for_people_who	4	17	f	t	2020-12-11 23:57:46.643973+08
59	14	looking_for_people_who	5	43	f	t	2020-12-11 23:57:46.643973+08
\.


--
-- Data for Name: help_group; Type: TABLE DATA; Schema: app_public; Owner: friensco
--

COPY app_public.help_group (help_id, virtual_user_id, created_at) FROM stdin;
39	1	2020-12-11 23:57:46.649558+08
40	1	2020-12-11 23:57:46.649558+08
41	1	2020-12-11 23:57:46.649558+08
39	7	2020-12-11 23:57:46.649558+08
40	7	2020-12-11 23:57:46.649558+08
41	7	2020-12-11 23:57:46.649558+08
42	6	2020-12-11 23:57:46.649558+08
43	2	2020-12-11 23:57:46.649558+08
44	3	2020-12-11 23:57:46.649558+08
45	3	2020-12-11 23:57:46.649558+08
46	4	2020-12-11 23:57:46.649558+08
47	5	2020-12-11 23:57:46.649558+08
48	6	2020-12-11 23:57:46.649558+08
49	7	2020-12-11 23:57:46.649558+08
50	8	2020-12-11 23:57:46.649558+08
51	9	2020-12-11 23:57:46.649558+08
52	10	2020-12-11 23:57:46.649558+08
53	11	2020-12-11 23:57:46.649558+08
54	12	2020-12-11 23:57:46.649558+08
55	13	2020-12-11 23:57:46.649558+08
56	14	2020-12-11 23:57:46.649558+08
57	15	2020-12-11 23:57:46.649558+08
58	16	2020-12-11 23:57:46.649558+08
59	17	2020-12-11 23:57:46.649558+08
48	18	2020-12-11 23:57:46.649558+08
48	19	2020-12-11 23:57:46.649558+08
50	20	2020-12-11 23:57:46.649558+08
49	20	2020-12-11 23:57:46.649558+08
12	20	2020-12-11 23:57:46.649558+08
7	20	2020-12-11 23:57:46.649558+08
43	20	2020-12-11 23:57:46.649558+08
\.


--
-- Data for Name: lazy_question; Type: TABLE DATA; Schema: app_public; Owner: friensco
--

COPY app_public.lazy_question (id, user_id, target_user_id, question, is_closed, searchable, created_at) FROM stdin;
1	7	\N	Should I buy iPhone 12?	f	t	2020-12-08 22:37:56.803536+08
2	7	\N	Anyone recommends good movie on Netflix?	f	t	2020-12-08 22:37:56.803536+08
3	7	\N	Uber or Lift? Which is better?	f	t	2020-12-08 22:37:56.803536+08
\.


--
-- Data for Name: object_of_help; Type: TABLE DATA; Schema: app_public; Owner: friensco
--

COPY app_public.object_of_help (id, name, created_at) FROM stdin;
1	profile	2020-12-08 22:37:56.803536+08
2	provide item	2020-12-08 22:37:56.803536+08
3	host activity	2020-12-08 22:37:56.803536+08
4	attend activity	2020-12-08 22:37:56.803536+08
5	know/experience	2020-12-08 22:37:56.803536+08
6	want pay together	2020-12-08 22:37:56.803536+08
\.


--
-- Data for Name: property_node; Type: TABLE DATA; Schema: app_public; Owner: friensco
--

COPY app_public.property_node (id, name, types, urls, description, is_private, created_at) FROM stdin;
1	chat	{}	{}		f	2020-12-08 22:37:56.803536+08
2	spirituality	{}	{}		f	2020-12-08 22:37:56.803536+08
3	politics	{}	{}		f	2020-12-08 22:37:56.803536+08
4	cooking	{}	{}		f	2020-12-08 22:37:56.803536+08
5	complaining boss	{}	{}		f	2020-12-08 22:37:56.803536+08
6	girl's talk	{}	{}		f	2020-12-08 22:37:56.803536+08
7	haagendazs	{"ice cream",food}	{}		f	2020-12-08 22:37:56.803536+08
8	supamop	{mop,houseware,cleaning}	{}		f	2020-12-08 22:37:56.803536+08
9	vitamin c	{vitamin,"dietary supplement",health}	{}		f	2020-12-08 22:37:56.803536+08
10	fish oils	{"dietary supplement",health}	{}		f	2020-12-08 22:37:56.803536+08
11	Atlas Shrugged	{book,novel,"Ayn Rand"}	{}		f	2020-12-08 22:37:56.803536+08
12	The History OF Western Philosophy	{book,philosophy,textbook,"Bertrand Russell"}	{}		f	2020-12-08 22:37:56.803536+08
13	hiking	{}	{}		f	2020-12-08 22:37:56.803536+08
14	dinner	{}	{}		f	2020-12-08 22:37:56.803536+08
15	dance	{}	{}		f	2020-12-08 22:37:56.803536+08
16	meditation	{spirituality,health}	{}		f	2020-12-08 22:37:56.803536+08
17	camping	{}	{}		f	2020-12-08 22:37:56.803536+08
18	fitness	{health,attractiveness}	{}		f	2020-12-08 22:37:56.803536+08
19	welcome a dating	{}	{}	I prefer a relationship which could lead to marriage. My ideal type of date is an extravert, age under 35, and wants to have kids.	f	2020-12-08 22:37:56.803536+08
20	welcome a dating	{}	{}	I'm looking for a man who is willing to support my career. I wish we can talk to other easily like good friends, and sometimes enjoy romantic moments.	f	2020-12-08 22:37:56.803536+08
21	welcome a dating	{}	{}	His age or mental age should be greater than 40. Handsome is important to me. He has to be rich enough	f	2020-12-08 22:37:56.803536+08
22	welcome a dating	{}	{}	Athlete, humorous, outgoing, like sports	f	2020-12-08 22:37:56.803536+08
23	welcome a dating	{}	{}	I'm open to make new friends or know more about old friends.	f	2020-12-08 22:37:56.803536+08
24	welcome a dating	{}	{}	a sincere relationship. no game. living each other and support each other. I like and have a dog. Hope you also like dogs	f	2020-12-08 22:37:56.803536+08
25	reactjs	{software,web,frontend}	{}		f	2020-12-08 22:37:56.803536+08
26	python	{programming,"data science"}	{}		f	2020-12-08 22:37:56.803536+08
27	coworking space	{"office management","community management"}	{}		f	2020-12-08 22:37:56.803536+08
28	cats	{}	{}	3 cats in my house. I love cats. Any friend can ask me anything about cats	f	2020-12-08 22:37:56.803536+08
29	yoga	{}	{}	I'm a yoga teacher.	f	2020-12-08 22:37:56.803536+08
30	Excel	{"office software"}	{}	I think I know how to use Microsoft Excel very much. welcome discussion!	f	2020-12-08 22:37:56.803536+08
31	sleep	{health}	{}	I had serious sleeping disorder. Now I have improved a lot and can share much of my experience.	f	2020-12-08 22:37:56.803536+08
32	meditation	{}	{}	I started to practice meditation since 10 years ago. Among the 10 years, I have accumulated about 3 years in which I do meditation as a habit.	f	2020-12-08 22:37:56.803536+08
33	depression	{}	{}	My job in medicine gives me knowledge about depression. Welcome to ask me if you have questions.	f	2020-12-08 22:37:56.803536+08
34	cockroach	{}	{}	I used to seeing tens of cockroach everyday in my house. Now I seldom see them. I can share how I did it	f	2020-12-08 22:37:56.803536+08
35	covid-19	{covid-19}	{}	We do researches on Covid-19 in our lab.	f	2020-12-08 22:37:56.803536+08
36	good restaurants in San Francisco	{}	{}	a super foodie living in San Francisco	f	2020-12-08 22:37:56.803536+08
37	cryptocurrency	{}	{}		f	2020-12-08 22:37:56.803536+08
38	China	{}	{}	I worked in China for 10 years.	f	2020-12-08 22:37:56.803536+08
39	taking care of kids	{family}	{}	I dare not say myself an expert, but I really really research a lot for raising my kids. I'm happy to share my experience with you	f	2020-12-08 22:37:56.803536+08
40	1980-02-23	{birthday}	{}		f	2020-12-08 22:37:56.803536+08
41	1980-04-13	{birthday}	{}		f	2020-12-08 22:37:56.803536+08
42	1980-11-18	{birthday}	{}		f	2020-12-08 22:37:56.803536+08
43	1981-08-21	{birthday}	{}		f	2020-12-08 22:37:56.803536+08
44	1982-03-17	{birthday}	{}		f	2020-12-08 22:37:56.803536+08
45	1983-10-17	{birthday}	{}		f	2020-12-08 22:37:56.803536+08
46	1984-03-26	{birthday}	{}		f	2020-12-08 22:37:56.803536+08
47	1985-10-07	{birthday}	{}		f	2020-12-08 22:37:56.803536+08
48	1988-05-03	{birthday}	{}		f	2020-12-08 22:37:56.803536+08
49	1989-02-10	{birthday}	{}		f	2020-12-08 22:37:56.803536+08
50	1992-12-02	{birthday}	{}		f	2020-12-08 22:37:56.803536+08
51	1993-03-23	{birthday}	{}		f	2020-12-08 22:37:56.803536+08
52	1996-07-04	{birthday}	{}		f	2020-12-08 22:37:56.803536+08
53	1998-06-01	{birthday}	{}		f	2020-12-08 22:37:56.803536+08
54	1998-10-23	{birthday}	{}		f	2020-12-08 22:37:56.803536+08
55	1999-11-17	{birthday}	{}		f	2020-12-08 22:37:56.803536+08
56	2000-06-19	{birthday}	{}		f	2020-12-08 22:37:56.803536+08
57	2000-07-25	{birthday}	{}		f	2020-12-08 22:37:56.803536+08
58	2001-09-28	{birthday}	{}		f	2020-12-08 22:37:56.803536+08
59	Female	{gender}	{}		f	2020-12-08 22:37:56.803536+08
60	Male	{gender}	{}		f	2020-12-08 22:37:56.803536+08
61	Master	{education}	{}		f	2020-12-08 22:37:56.803536+08
62	College	{education}	{}		f	2020-12-08 22:37:56.803536+08
63	Upper secondary	{education}	{}		f	2020-12-08 22:37:56.803536+08
64	single	{"relationship status"}	{}		f	2020-12-08 22:37:56.803536+08
65	married	{"relationship status"}	{}		f	2020-12-08 22:37:56.803536+08
66	in a relationship	{"relationship status"}	{}		f	2020-12-08 22:37:56.803536+08
67	1	{"number of children"}	{}		f	2020-12-08 22:37:56.803536+08
68	2	{"number of children"}	{}		f	2020-12-08 22:37:56.803536+08
69	3	{"number of children"}	{}		f	2020-12-08 22:37:56.803536+08
70	Auditor	{occupation}	{}		f	2020-12-08 22:37:56.803536+08
71	Geologist	{occupation}	{}		f	2020-12-08 22:37:56.803536+08
72	Photographer	{occupation}	{}		f	2020-12-08 22:37:56.803536+08
73	Historian	{occupation}	{}		f	2020-12-08 22:37:56.803536+08
74	Astronomer	{occupation}	{}		f	2020-12-08 22:37:56.803536+08
75	Aeroplane Pilot	{occupation}	{}		f	2020-12-08 22:37:56.803536+08
76	Police Officer	{occupation}	{}		f	2020-12-08 22:37:56.803536+08
77	Social Worker	{occupation}	{}		f	2020-12-08 22:37:56.803536+08
78	Biochemist	{occupation}	{}		f	2020-12-08 22:37:56.803536+08
79	Accountant	{occupation}	{}		f	2020-12-08 22:37:56.803536+08
80	Angel Investor	{}	{}		f	2020-12-08 22:37:56.803536+08
81	startup	{}	{}		f	2020-12-08 22:37:56.803536+08
82	Model	{occupation}	{}		f	2020-12-08 22:37:56.803536+08
83	rent in Taipei	{accommodation,roommate}	{}	about 20k NTD per person. Kitchen and well functioning air conditioner required.	f	2020-12-08 22:37:56.803536+08
84	rent in SF Bay Area	{accommodation,roommate}	{}	Private rooms, good Internet.	f	2020-12-08 22:37:56.803536+08
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: app_public; Owner: friensco
--

COPY app_public.users (id, name, email, avatar_url, cellphone, created_at, updated_at, is_verified, who_you_are) FROM stdin;
1	Shaka	scchen@shaka.today	https://robohash.org/IeBai5ei	\N	2020-12-08 22:37:56.803536+08	2020-12-08 22:37:56.803536+08	f	
2	Eveline	eveline@eveline.eveline	https://robohash.org/eiBie4au	\N	2020-12-08 22:37:56.803536+08	2020-12-08 22:37:56.803536+08	f	
3	stone man	stone@stoneman.tw	https://robohash.org/chahL8eb	\N	2020-12-08 22:37:56.803536+08	2020-12-08 22:37:56.803536+08	f	
4	Cherish Beard	zwood@live.com	https://robohash.org/Vei2thei	\N	2020-12-08 22:37:56.803536+08	2020-12-08 22:37:56.803536+08	f	
5	Yusuf Blackwell	vertigo@yahoo.com	https://robohash.org/Chai6Que	\N	2020-12-08 22:37:56.803536+08	2020-12-08 22:37:56.803536+08	f	
6	Janessa Gonzalez	thrymm@yahoo.ca	https://robohash.org/oobu1Equ	\N	2020-12-08 22:37:56.803536+08	2020-12-08 22:37:56.803536+08	f	
7	Carolina Blevins	bmcmahon@verizon.net	https://robohash.org/Ieghai0g	\N	2020-12-08 22:37:56.803536+08	2020-12-08 22:37:56.803536+08	f	
8	Jordyn Curry	webteam@icloud.com	https://robohash.org/ahvius7G	\N	2020-12-08 22:37:56.803536+08	2020-12-08 22:37:56.803536+08	f	
9	Jovanny Anderson	granboul@mac.com	https://robohash.org/Naefoh5j	\N	2020-12-08 22:37:56.803536+08	2020-12-08 22:37:56.803536+08	f	
10	Cameron Jordan	jonadab@aol.com	https://robohash.org/ih0Eep0t	\N	2020-12-08 22:37:56.803536+08	2020-12-08 22:37:56.803536+08	f	
11	Jack Williamson	balchen@aol.com	https://robohash.org/li1GahNa	\N	2020-12-08 22:37:56.803536+08	2020-12-08 22:37:56.803536+08	f	
12	Osvaldo Knapp	rattenbt@live.com	https://robohash.org/Ni3nahng	\N	2020-12-08 22:37:56.803536+08	2020-12-08 22:37:56.803536+08	f	
13	Deja Cantu	sinkou@live.com	https://robohash.org/shei2Jai	\N	2020-12-08 22:37:56.803536+08	2020-12-08 22:37:56.803536+08	f	
14	Quinn Carter	markjugg@gmail.com	https://robohash.org/Iebie7Gi	\N	2020-12-08 22:37:56.803536+08	2020-12-08 22:37:56.803536+08	f	
15	Rudy Dixon	mugwump@optonline.net	https://robohash.org/eChaoth3	\N	2020-12-08 22:37:56.803536+08	2020-12-08 22:37:56.803536+08	f	
\.


--
-- Data for Name: virtual_user; Type: TABLE DATA; Schema: app_public; Owner: friensco
--

COPY app_public.virtual_user (id, user_id, target_user_id, name, display_name, can_do_referral, created_at) FROM stdin;
1	1	2	Carmen Phelps	Dominique Conway	f	2020-12-11 23:57:46.619328+08
2	1	2	Jimmie Padilla	Gene Mccullough	f	2020-12-11 23:57:46.619328+08
3	2	2	Essie Sexton	Milo Mccarty	f	2020-12-11 23:57:46.619328+08
4	2	1	Silas Bowman	Hallie Rios	f	2020-12-11 23:57:46.619328+08
5	1	1	Doreen Dillon	Doreen Bowman	f	2020-12-11 23:57:46.619328+08
6	1	3	Cody Harmon	Tamera Greene	t	2020-12-11 23:57:46.626491+08
7	1	4	Julia Maynard	Heidi Klein	t	2020-12-11 23:57:46.626491+08
8	3	3	Harley Daniels	Myron Mccarthy	t	2020-12-11 23:57:46.626491+08
9	3	1	Roberto Cox	Miriam Kennedy	t	2020-12-11 23:57:46.626491+08
10	1	1	Jess Stein	Israel Moody	t	2020-12-11 23:57:46.626491+08
11	1	\N	Norman Everett	Roxie Cantrell	f	2020-12-11 23:57:46.631695+08
12	1	\N	Mari Finley	Hallie Todd	f	2020-12-11 23:57:46.631695+08
13	1	\N	Richard Torres	Wilford Sweet	f	2020-12-11 23:57:46.631695+08
14	2	\N	Chester Morales	Pam Frost	f	2020-12-11 23:57:46.631695+08
15	2	\N	Serena Cantu	Geoffrey Mcmahon	f	2020-12-11 23:57:46.631695+08
16	1	\N	Brad Chang	Lina Mejia	t	2020-12-11 23:57:46.635892+08
17	1	\N	Diann Phillips	Dee Simpson	t	2020-12-11 23:57:46.635892+08
18	2	\N	Alexandra Puckett	Fredric Macdonald	t	2020-12-11 23:57:46.635892+08
19	2	\N	Candice Craft	Rod Sykes	t	2020-12-11 23:57:46.635892+08
20	3	\N	Karina Pugh	Terrence Gay	t	2020-12-11 23:57:46.635892+08
\.


--
-- Name: help_id_seq; Type: SEQUENCE SET; Schema: app_public; Owner: friensco
--

SELECT pg_catalog.setval('app_public.help_id_seq', 59, true);


--
-- Name: lazy_question_id_seq; Type: SEQUENCE SET; Schema: app_public; Owner: friensco
--

SELECT pg_catalog.setval('app_public.lazy_question_id_seq', 3, true);


--
-- Name: object_of_help_id_seq; Type: SEQUENCE SET; Schema: app_public; Owner: friensco
--

SELECT pg_catalog.setval('app_public.object_of_help_id_seq', 6, true);


--
-- Name: property_node_id_seq; Type: SEQUENCE SET; Schema: app_public; Owner: friensco
--

SELECT pg_catalog.setval('app_public.property_node_id_seq', 84, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: app_public; Owner: friensco
--

SELECT pg_catalog.setval('app_public.users_id_seq', 15, true);


--
-- Name: virtual_user_id_seq; Type: SEQUENCE SET; Schema: app_public; Owner: friensco
--

SELECT pg_catalog.setval('app_public.virtual_user_id_seq', 20, true);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: app_private; Owner: friensco
--

ALTER TABLE ONLY app_private.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: friendship friendship_pkey; Type: CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.friendship
    ADD CONSTRAINT friendship_pkey PRIMARY KEY (source_id, target_id);


--
-- Name: help_group help_group_pkey; Type: CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.help_group
    ADD CONSTRAINT help_group_pkey PRIMARY KEY (help_id, virtual_user_id);


--
-- Name: help help_pkey; Type: CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.help
    ADD CONSTRAINT help_pkey PRIMARY KEY (id);


--
-- Name: lazy_question lazy_question_pkey; Type: CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.lazy_question
    ADD CONSTRAINT lazy_question_pkey PRIMARY KEY (id);


--
-- Name: object_of_help object_of_help_pkey; Type: CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.object_of_help
    ADD CONSTRAINT object_of_help_pkey PRIMARY KEY (id);


--
-- Name: property_node property_node_name_types_urls_description_key; Type: CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.property_node
    ADD CONSTRAINT property_node_name_types_urls_description_key UNIQUE (name, types, urls, description);


--
-- Name: property_node property_node_pkey; Type: CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.property_node
    ADD CONSTRAINT property_node_pkey PRIMARY KEY (id);


--
-- Name: users users_avatar_url_key; Type: CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.users
    ADD CONSTRAINT users_avatar_url_key UNIQUE (avatar_url);


--
-- Name: users users_cellphone_key; Type: CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.users
    ADD CONSTRAINT users_cellphone_key UNIQUE (cellphone);


--
-- Name: users users_email_key; Type: CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.users
    ADD CONSTRAINT users_email_key UNIQUE (email);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: virtual_user virtual_user_pkey; Type: CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.virtual_user
    ADD CONSTRAINT virtual_user_pkey PRIMARY KEY (id);


--
-- Name: virtual_user virtual_user_user_id_display_name_key; Type: CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.virtual_user
    ADD CONSTRAINT virtual_user_user_id_display_name_key UNIQUE (user_id, display_name);


--
-- Name: virtual_user virtual_user_user_id_name_key; Type: CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.virtual_user
    ADD CONSTRAINT virtual_user_user_id_name_key UNIQUE (user_id, name);


--
-- Name: contact_address_target_user_id_idx; Type: INDEX; Schema: app_public; Owner: friensco
--

CREATE INDEX contact_address_target_user_id_idx ON app_public.contact_address USING btree (target_user_id);


--
-- Name: contact_address_user_id_idx; Type: INDEX; Schema: app_public; Owner: friensco
--

CREATE INDEX contact_address_user_id_idx ON app_public.contact_address USING btree (user_id);


--
-- Name: friendship_target_id_idx; Type: INDEX; Schema: app_public; Owner: friensco
--

CREATE INDEX friendship_target_id_idx ON app_public.friendship USING btree (target_id);


--
-- Name: help_group_virtual_user_id_idx; Type: INDEX; Schema: app_public; Owner: friensco
--

CREATE INDEX help_group_virtual_user_id_idx ON app_public.help_group USING btree (virtual_user_id);


--
-- Name: help_help_attitude_object_of_help_id_property_id_idx; Type: INDEX; Schema: app_public; Owner: friensco
--

CREATE INDEX help_help_attitude_object_of_help_id_property_id_idx ON app_public.help USING btree (help_attitude, object_of_help_id, property_id);


--
-- Name: help_object_of_help_id_idx; Type: INDEX; Schema: app_public; Owner: friensco
--

CREATE INDEX help_object_of_help_id_idx ON app_public.help USING btree (object_of_help_id);


--
-- Name: help_property_id_idx; Type: INDEX; Schema: app_public; Owner: friensco
--

CREATE INDEX help_property_id_idx ON app_public.help USING btree (property_id);


--
-- Name: help_user_id_idx; Type: INDEX; Schema: app_public; Owner: friensco
--

CREATE INDEX help_user_id_idx ON app_public.help USING btree (user_id);


--
-- Name: lazy_question_target_user_id_idx; Type: INDEX; Schema: app_public; Owner: friensco
--

CREATE INDEX lazy_question_target_user_id_idx ON app_public.lazy_question USING btree (target_user_id);


--
-- Name: lazy_question_user_id_idx; Type: INDEX; Schema: app_public; Owner: friensco
--

CREATE INDEX lazy_question_user_id_idx ON app_public.lazy_question USING btree (user_id);


--
-- Name: virtual_user_target_user_id_idx; Type: INDEX; Schema: app_public; Owner: friensco
--

CREATE INDEX virtual_user_target_user_id_idx ON app_public.virtual_user USING btree (target_user_id);


--
-- Name: users users_id_fkey; Type: FK CONSTRAINT; Schema: app_private; Owner: friensco
--

ALTER TABLE ONLY app_private.users
    ADD CONSTRAINT users_id_fkey FOREIGN KEY (id) REFERENCES app_public.users(id) ON DELETE CASCADE;


--
-- Name: contact_address contact_address_target_user_id_fkey; Type: FK CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.contact_address
    ADD CONSTRAINT contact_address_target_user_id_fkey FOREIGN KEY (target_user_id) REFERENCES app_public.users(id);


--
-- Name: contact_address contact_address_user_id_fkey; Type: FK CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.contact_address
    ADD CONSTRAINT contact_address_user_id_fkey FOREIGN KEY (user_id) REFERENCES app_public.users(id);


--
-- Name: friendship friendship_source_id_fkey; Type: FK CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.friendship
    ADD CONSTRAINT friendship_source_id_fkey FOREIGN KEY (source_id) REFERENCES app_public.users(id);


--
-- Name: friendship friendship_target_id_fkey; Type: FK CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.friendship
    ADD CONSTRAINT friendship_target_id_fkey FOREIGN KEY (target_id) REFERENCES app_public.users(id);


--
-- Name: help_group help_group_help_id_fkey; Type: FK CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.help_group
    ADD CONSTRAINT help_group_help_id_fkey FOREIGN KEY (help_id) REFERENCES app_public.help(id);


--
-- Name: help_group help_group_virtual_user_id_fkey; Type: FK CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.help_group
    ADD CONSTRAINT help_group_virtual_user_id_fkey FOREIGN KEY (virtual_user_id) REFERENCES app_public.virtual_user(id);


--
-- Name: help help_object_of_help_id_fkey; Type: FK CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.help
    ADD CONSTRAINT help_object_of_help_id_fkey FOREIGN KEY (object_of_help_id) REFERENCES app_public.object_of_help(id);


--
-- Name: help help_property_id_fkey; Type: FK CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.help
    ADD CONSTRAINT help_property_id_fkey FOREIGN KEY (property_id) REFERENCES app_public.property_node(id);


--
-- Name: help help_user_id_fkey; Type: FK CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.help
    ADD CONSTRAINT help_user_id_fkey FOREIGN KEY (user_id) REFERENCES app_public.users(id);


--
-- Name: lazy_question lazy_question_target_user_id_fkey; Type: FK CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.lazy_question
    ADD CONSTRAINT lazy_question_target_user_id_fkey FOREIGN KEY (target_user_id) REFERENCES app_public.users(id);


--
-- Name: lazy_question lazy_question_user_id_fkey; Type: FK CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.lazy_question
    ADD CONSTRAINT lazy_question_user_id_fkey FOREIGN KEY (user_id) REFERENCES app_public.users(id);


--
-- Name: virtual_user virtual_user_target_user_id_fkey; Type: FK CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.virtual_user
    ADD CONSTRAINT virtual_user_target_user_id_fkey FOREIGN KEY (target_user_id) REFERENCES app_public.users(id);


--
-- Name: virtual_user virtual_user_user_id_fkey; Type: FK CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.virtual_user
    ADD CONSTRAINT virtual_user_user_id_fkey FOREIGN KEY (user_id) REFERENCES app_public.users(id);


--
-- Name: SCHEMA app_private; Type: ACL; Schema: -; Owner: friensco
--

GRANT USAGE ON SCHEMA app_private TO friensco_visitor;


--
-- Name: SCHEMA app_public; Type: ACL; Schema: -; Owner: friensco
--

GRANT USAGE ON SCHEMA app_public TO friensco_user;
GRANT USAGE ON SCHEMA app_public TO friensco_visitor;


--
-- Name: TABLE users; Type: ACL; Schema: app_public; Owner: friensco
--

GRANT ALL ON TABLE app_public.users TO friensco_user;


--
-- Name: FUNCTION authenticate(email text, password text); Type: ACL; Schema: app_public; Owner: friensco
--

GRANT ALL ON FUNCTION app_public.authenticate(email text, password text) TO friensco_user;
GRANT ALL ON FUNCTION app_public.authenticate(email text, password text) TO friensco_visitor;


--
-- Name: FUNCTION get_current_user_id(); Type: ACL; Schema: app_public; Owner: friensco
--

GRANT ALL ON FUNCTION app_public.get_current_user_id() TO friensco_user;


--
-- Name: TABLE help; Type: ACL; Schema: app_public; Owner: friensco
--

GRANT ALL ON TABLE app_public.help TO friensco_user;


--
-- Name: TABLE virtual_user; Type: ACL; Schema: app_public; Owner: friensco
--

GRANT ALL ON TABLE app_public.virtual_user TO friensco_user;


--
-- Name: TABLE contact_address; Type: ACL; Schema: app_public; Owner: friensco
--

GRANT ALL ON TABLE app_public.contact_address TO friensco_user;


--
-- Name: TABLE friendship; Type: ACL; Schema: app_public; Owner: friensco
--

GRANT ALL ON TABLE app_public.friendship TO friensco_user;


--
-- Name: TABLE help_group; Type: ACL; Schema: app_public; Owner: friensco
--

GRANT ALL ON TABLE app_public.help_group TO friensco_user;


--
-- Name: TABLE lazy_question; Type: ACL; Schema: app_public; Owner: friensco
--

GRANT ALL ON TABLE app_public.lazy_question TO friensco_user;


--
-- Name: TABLE object_of_help; Type: ACL; Schema: app_public; Owner: friensco
--

GRANT ALL ON TABLE app_public.object_of_help TO friensco_user;


--
-- Name: TABLE property_node; Type: ACL; Schema: app_public; Owner: friensco
--

GRANT ALL ON TABLE app_public.property_node TO friensco_user;


--
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: app_public; Owner: danielviolin
--

ALTER DEFAULT PRIVILEGES FOR ROLE danielviolin IN SCHEMA app_public REVOKE ALL ON TABLES  FROM danielviolin;
ALTER DEFAULT PRIVILEGES FOR ROLE danielviolin IN SCHEMA app_public GRANT ALL ON TABLES  TO friensco_user;


--
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: public; Owner: danielviolin
--

ALTER DEFAULT PRIVILEGES FOR ROLE danielviolin IN SCHEMA public REVOKE ALL ON TABLES  FROM danielviolin;
ALTER DEFAULT PRIVILEGES FOR ROLE danielviolin IN SCHEMA public GRANT ALL ON TABLES  TO friensco_user;


--
-- PostgreSQL database dump complete
--


-- virtual user targeting real user
INSERT INTO app_public.virtual_user (user_id, NAME, display_name, can_do_referral, target_user_id)
  VALUES (1, 'Carmen Phelps', 'Dominique Conway', FALSE, 2), (1, 'Jimmie Padilla', 'Gene Mccullough', FALSE, 2), (2, 'Essie Sexton', 'Milo Mccarty', FALSE, 2), (2, 'Silas Bowman', 'Hallie Rios', FALSE, 1), (1, 'Doreen Dillon', 'Doreen Bowman', FALSE, 1);

INSERT INTO app_public.virtual_user (user_id, NAME, display_name, can_do_referral, target_user_id)
  VALUES (1, 'Cody Harmon', 'Tamera Greene', TRUE, 3), (1, 'Julia Maynard', 'Heidi Klein', TRUE, 4), (3, 'Harley Daniels', 'Myron Mccarthy', TRUE, 3), (3, 'Roberto Cox', 'Miriam Kennedy', TRUE, 1), (1, 'Jess Stein', 'Israel Moody', TRUE, 1);

-- virtual user without targeting real user
INSERT INTO app_public.virtual_user (user_id, NAME, display_name, can_do_referral)
  VALUES (1, 'Norman Everett', 'Roxie Cantrell', FALSE), (1, 'Mari Finley', 'Hallie Todd', FALSE), (1, 'Richard Torres', 'Wilford Sweet', FALSE), (2, 'Chester Morales', 'Pam Frost', FALSE), (2, 'Serena Cantu', 'Geoffrey Mcmahon', FALSE);

INSERT INTO app_public.virtual_user (user_id, NAME, display_name, can_do_referral)
  VALUES (1, 'Brad Chang', 'Lina Mejia', TRUE), (1, 'Diann Phillips', 'Dee Simpson', TRUE), (2, 'Alexandra Puckett', 'Fredric Macdonald', TRUE), (2, 'Candice Craft', 'Rod Sykes', TRUE), (3, 'Karina Pugh', 'Terrence Gay', TRUE);

INSERT INTO app_public.help (user_id, help_attitude, object_of_help_id, property_id)
  VALUES (1, 'looking_for_people_who', 4, 6), (1, 'looking_for_people_who', 1, 22), (1, 'is_a_person_who', 1, 80), (1, 'looking_for_people_who', 1, 24);


/* next help.id should be 43 */
-- data with higher degree of random
INSERT INTO app_public.help (user_id, help_attitude, object_of_help_id, property_id)
  VALUES (
    /*43*/
    1, 'is_a_person_who', 2, 33), (
    /*44*/
    1, 'looking_for_people_who', 3, 27), (
    /*45*/
    1, 'is_a_person_who', 1, 70), (
    /*46*/
    1, 'looking_for_people_who', 4, 50), (
    /*47*/
    5, 'is_a_person_who', 5, 5), (
    /*48*/
    6, 'looking_for_people_who', 6, 10), (
    /*49*/
    7, 'is_a_person_who', 1, 23), (
    /*50*/
    7, 'looking_for_people_who', 2, 15), (
    /*51*/
    7, 'is_a_person_who', 3, 2), (
    /*52*/
    8, 'looking_for_people_who', 4, 38), (
    /*53*/
    9, 'is_a_person_who', 5, 19), (
    /*54*/
    12, 'looking_for_people_who', 6, 62), (
    /*55*/
    12, 'is_a_person_who', 1, 80), (
    /*56*/
    12, 'looking_for_people_who', 2, 12), (
    /*57*/
    12, 'is_a_person_who', 3, 4), (
    /*58*/
    14, 'looking_for_people_who', 4, 17), (
    /*59*/
    14, 'looking_for_people_who', 5, 43);

INSERT INTO app_public.help_group (help_id, virtual_user_id)
  VALUES (39, 1), (40, 1), (41, 1), (39, 7), (40, 7), (41, 7), (42, 6), (43, 2), (44, 3), (45, 3), (46, 4), (47, 5), (48, 6), (49, 7), (50, 8), (51, 9), (52, 10), (53, 11), (54, 12), (55, 13), (56, 14), (57, 15), (58, 16), (59, 17), (48, 18), (48, 19), (50, 20), (49, 20), (12, 20), (7, 20), (43, 20);


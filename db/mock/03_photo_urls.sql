UPDATE
  app_public.users
SET
  featured_photo_urls = ARRAY['https://picsum.photos/id/1027/284/427.jpg', 'https://picsum.photos/id/1011/547/364.jpg', 'https://picsum.photos/id/1006/300/200.jpg']
WHERE
  ID = 2;

UPDATE
  app_public.users
SET
  featured_photo_urls = ARRAY['https://picsum.photos/id/1005/576/384.jpg', 'https://picsum.photos/id/1009/500/750.jpg', 'https://picsum.photos/id/103/259/193.jpg', 'https://picsum.photos/id/1042/345/518.jpg', 'https://picsum.photos/id/1050/600/400.jpg']
WHERE
  ID = 3;

UPDATE
  app_public.virtual_user
SET
  featured_photo_urls = ARRAY['https://picsum.photos/300/200.jpg', 'https://picsum.photos/300/200.jpg', 'https://picsum.photos/300/200.jpg', 'https://picsum.photos/300/200.jpg']
WHERE
  ID = 8;

UPDATE
  app_public.virtual_user
SET
  featured_photo_urls = ARRAY['https://picsum.photos/300/200.jpg', 'https://picsum.photos/300/200.jpg', 'https://picsum.photos/300/200.jpg', 'https://picsum.photos/300/200.jpg', 'https://picsum.photos/300/200.jpg', 'https://picsum.photos/300/200.jpg']
WHERE
  ID = 18;

UPDATE
  app_public.property
SET
  urls = ARRAY['https://picsum.photos/300/200.jpg', 'https://picsum.photos/300/200.jpg']
WHERE
  ID % 2 = 0;

UPDATE
  app_public.property
SET
  urls = ARRAY['https://picsum.photos/200/300.jpg', 'https://picsum.photos/300/200.jpg', 'https://picsum.photos/400/300.jpg']
WHERE
  ID % 2 = 1;


--
-- PostgreSQL database dump
--

-- Dumped from database version 12.5
-- Dumped by pg_dump version 12.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE friensco_test_db;
--
-- Name: friensco_test_db; Type: DATABASE; Schema: -; Owner: friensco
--

CREATE DATABASE friensco_test_db WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';


ALTER DATABASE friensco_test_db OWNER TO friensco;

\connect friensco_test_db

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: app_hidden; Type: SCHEMA; Schema: -; Owner: friensco
--

CREATE SCHEMA app_hidden;


ALTER SCHEMA app_hidden OWNER TO friensco;

--
-- Name: app_private; Type: SCHEMA; Schema: -; Owner: friensco
--

CREATE SCHEMA app_private;


ALTER SCHEMA app_private OWNER TO friensco;

--
-- Name: app_public; Type: SCHEMA; Schema: -; Owner: friensco
--

CREATE SCHEMA app_public;


ALTER SCHEMA app_public OWNER TO friensco;

--
-- Name: hstore; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA public;


--
-- Name: EXTENSION hstore; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION hstore IS 'data type for storing sets of (key, value) pairs';


--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


--
-- Name: address_type; Type: TYPE; Schema: app_public; Owner: friensco
--

CREATE TYPE app_public.address_type AS ENUM (
    'cellphone',
    'fax',
    'fixedline_telephone',
    'email',
    'mail',
    'real_estate',
    'others'
);


ALTER TYPE app_public.address_type OWNER TO friensco;

--
-- Name: help_attitude; Type: TYPE; Schema: app_public; Owner: friensco
--

CREATE TYPE app_public.help_attitude AS ENUM (
    'call_for_help',
    'about_me'
);


ALTER TYPE app_public.help_attitude OWNER TO friensco;

--
-- Name: jwt_token; Type: TYPE; Schema: app_public; Owner: friensco
--

CREATE TYPE app_public.jwt_token AS (
	role text,
	exp integer,
	user_id integer,
	username character varying
);


ALTER TYPE app_public.jwt_token OWNER TO friensco;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: users; Type: TABLE; Schema: app_public; Owner: friensco
--

CREATE TABLE app_public.users (
    id integer NOT NULL,
    name character varying NOT NULL,
    email character varying NOT NULL,
    avatar_url character varying NOT NULL,
    cellphone character varying,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    is_verified boolean DEFAULT false NOT NULL,
    featured_photo_urls character varying[]
);


ALTER TABLE app_public.users OWNER TO friensco;

--
-- Name: manually_set_verified(integer); Type: FUNCTION; Schema: app_hidden; Owner: friensco
--

CREATE FUNCTION app_hidden.manually_set_verified(user_id integer) RETURNS app_public.users
    LANGUAGE sql STRICT
    AS $$
  UPDATE
    app_public.users
  SET
    is_verified = TRUE
  WHERE
    app_public.users.ID = user_id;

INSERT INTO app_public.friendship (source_id, target_id)
  VALUES (1, user_id), (user_id, 1);

SELECT
  *
FROM
  app_public.users
WHERE
  app_public.users.ID = user_id
$$;


ALTER FUNCTION app_hidden.manually_set_verified(user_id integer) OWNER TO friensco;

--
-- Name: authenticate(text, text); Type: FUNCTION; Schema: app_public; Owner: friensco
--

CREATE FUNCTION app_public.authenticate(email text, password text) RETURNS app_public.jwt_token
    LANGUAGE plpgsql STRICT SECURITY DEFINER
    AS $$
DECLARE
  user_password_salted TEXT;
  user_email VARCHAR;
  user_name VARCHAR;
  user_id INT;
BEGIN
  SELECT
    app_public.users.ID,
    app_public.users.email,
    app_public.users.NAME,
    app_private.users.password_salted INTO user_id,
    user_email,
    user_name,
    user_password_salted
  FROM
    app_private.users
    INNER JOIN app_public.users ON app_private.users.ID = app_public.users.ID
  WHERE
    app_public.users.email = authenticate.email;
  IF user_password_salted = crypt(PASSWORD, user_password_salted) THEN
    RETURN ('friensco_user',
      extract(epoch FROM now() + interval '7 days'),
      user_id,
      user_name)::app_public.jwt_token;
  ELSE
    RETURN NULL;
  END IF;
END;
$$;


ALTER FUNCTION app_public.authenticate(email text, password text) OWNER TO friensco;

--
-- Name: get_current_user(); Type: FUNCTION; Schema: app_public; Owner: friensco
--

CREATE FUNCTION app_public.get_current_user() RETURNS app_public.users
    LANGUAGE sql STABLE
    AS $$
  SELECT
    *
  FROM
    app_public.users
  WHERE
    ID = current_setting('jwt.claims.user_id', TRUE)::INT
  LIMIT 1;

$$;


ALTER FUNCTION app_public.get_current_user() OWNER TO friensco;

--
-- Name: get_current_user_id(); Type: FUNCTION; Schema: app_public; Owner: friensco
--

CREATE FUNCTION app_public.get_current_user_id() RETURNS integer
    LANGUAGE sql STABLE
    AS $$
  SELECT
    current_setting('jwt.claims.user_id', TRUE)::int;

$$;


ALTER FUNCTION app_public.get_current_user_id() OWNER TO friensco;

--
-- Name: property; Type: TABLE; Schema: app_public; Owner: friensco
--

CREATE TABLE app_public.property (
    id integer NOT NULL,
    user_id integer NOT NULL,
    help_attitude app_public.help_attitude NOT NULL,
    is_closed boolean DEFAULT false NOT NULL,
    searchable boolean DEFAULT true NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    tags character varying[] DEFAULT ARRAY[]::character varying[] NOT NULL,
    urls character varying[] DEFAULT ARRAY[]::character varying[] NOT NULL,
    description text DEFAULT ''::text NOT NULL,
    is_private boolean DEFAULT false NOT NULL
);


ALTER TABLE app_public.property OWNER TO friensco;

--
-- Name: property_is_owned_by_virtual_user(app_public.property); Type: FUNCTION; Schema: app_public; Owner: friensco
--

CREATE FUNCTION app_public.property_is_owned_by_virtual_user(property_record app_public.property) RETURNS boolean
    LANGUAGE sql STABLE
    AS $$
  SELECT
    (0 < (
        SELECT
          COUNT(*)
        FROM
          app_public.property_group
        WHERE
          property_record.ID = app_public.property_group.property_id))
$$;


ALTER FUNCTION app_public.property_is_owned_by_virtual_user(property_record app_public.property) OWNER TO friensco;

--
-- Name: signup(character varying, character varying, text); Type: FUNCTION; Schema: app_public; Owner: friensco
--

CREATE FUNCTION app_public.signup(email character varying, name character varying, password text) RETURNS app_public.jwt_token
    LANGUAGE plpgsql STRICT SECURITY DEFINER
    AS $$
DECLARE
  user_password_salted TEXT = crypt(PASSWORD, gen_salt('bf'));
  user_email VARCHAR;
  user_name VARCHAR;
  user_id INT;
  alpha_user_email varchar;
BEGIN
  WITH new_user AS (
INSERT INTO app_public.users (email, NAME, avatar_url)
      VALUES (email, NAME, 'https://robohash.org/' || SUBSTRING(md5(now()::TEXT), 1, 8))
    RETURNING
      app_public.users.ID, app_public.users.NAME, app_public.users.email)
    SELECT
      new_user.ID,
      new_user.NAME,
      new_user.email INTO user_id,
      user_name,
      user_email
    FROM
      new_user;
  INSERT INTO app_private.users (ID, password_salted)
    VALUES (user_id, user_password_salted);
  IF (user_email,
    user_name,
    user_id) IS NOT NULL THEN
    INSERT INTO app_public.friendship (source_id, target_id)
      VALUES (1, user_id), (user_id, 1);
    RETURN ('friensco_user',
      extract(epoch FROM now() + interval '7 days'),
      user_id,
      user_name)::app_public.jwt_token;
  END IF;
  RETURN NULL;
END;
$$;


ALTER FUNCTION app_public.signup(email character varying, name character varying, password text) OWNER TO friensco;

--
-- Name: virtual_user; Type: TABLE; Schema: app_public; Owner: friensco
--

CREATE TABLE app_public.virtual_user (
    id integer NOT NULL,
    user_id integer NOT NULL,
    target_user_id integer,
    name character varying NOT NULL,
    display_name character varying NOT NULL,
    can_do_referral boolean DEFAULT false NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    featured_photo_urls character varying[]
);


ALTER TABLE app_public.virtual_user OWNER TO friensco;

--
-- Name: virtual_users_who_friends_can_do_referral_to_current_user(); Type: FUNCTION; Schema: app_public; Owner: friensco
--

CREATE FUNCTION app_public.virtual_users_who_friends_can_do_referral_to_current_user() RETURNS SETOF app_public.virtual_user
    LANGUAGE plpgsql STABLE
    AS $$
DECLARE
  current_user_id INT := current_setting('jwt.claims.user_id', TRUE)::INT;
BEGIN
  RETURN query
  SELECT
    *
  FROM
    app_public.virtual_user
  WHERE
    app_public.virtual_user.user_id IN (
      SELECT
        app_public.friendship.target_id
      FROM
        app_public.friendship
      WHERE
        app_public.friendship.source_id = current_user_id)
    AND app_public.virtual_user.can_do_referral = TRUE;
END;
$$;


ALTER FUNCTION app_public.virtual_users_who_friends_can_do_referral_to_current_user() OWNER TO friensco;

--
-- Name: users; Type: TABLE; Schema: app_private; Owner: friensco
--

CREATE TABLE app_private.users (
    id integer NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    password_salted text NOT NULL
);


ALTER TABLE app_private.users OWNER TO friensco;

--
-- Name: direct_message; Type: TABLE; Schema: app_public; Owner: friensco
--

CREATE TABLE app_public.direct_message (
    id integer NOT NULL,
    user_id integer NOT NULL,
    target_user_id integer NOT NULL,
    text text NOT NULL,
    image_url character varying,
    video_url character varying,
    audio_url character varying,
    created_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE app_public.direct_message OWNER TO friensco;

--
-- Name: direct_message_id_seq; Type: SEQUENCE; Schema: app_public; Owner: friensco
--

ALTER TABLE app_public.direct_message ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME app_public.direct_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: friendship; Type: TABLE; Schema: app_public; Owner: friensco
--

CREATE TABLE app_public.friendship (
    source_id integer NOT NULL,
    target_id integer NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT friendship_check CHECK ((source_id <> target_id))
);


ALTER TABLE app_public.friendship OWNER TO friensco;

--
-- Name: help_id_seq; Type: SEQUENCE; Schema: app_public; Owner: friensco
--

ALTER TABLE app_public.property ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME app_public.help_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: property_group; Type: TABLE; Schema: app_public; Owner: friensco
--

CREATE TABLE app_public.property_group (
    property_id integer NOT NULL,
    virtual_user_id integer NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE app_public.property_group OWNER TO friensco;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: app_public; Owner: friensco
--

ALTER TABLE app_public.users ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME app_public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: virtual_user_id_seq; Type: SEQUENCE; Schema: app_public; Owner: friensco
--

ALTER TABLE app_public.virtual_user ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME app_public.virtual_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Data for Name: users; Type: TABLE DATA; Schema: app_private; Owner: friensco
--

COPY app_private.users (id, updated_at, password_salted) FROM stdin;
1	2020-12-08 22:37:56.803536+08	$2a$08$PQLZW88x2lsugsZ3Nr5LTeIu0LLvPwRqQ1Uja8OgeTBUnGpH.1F0m
\.


--
-- Data for Name: direct_message; Type: TABLE DATA; Schema: app_public; Owner: friensco
--

COPY app_public.direct_message (id, user_id, target_user_id, text, image_url, video_url, audio_url, created_at) FROM stdin;
\.


--
-- Data for Name: friendship; Type: TABLE DATA; Schema: app_public; Owner: friensco
--

COPY app_public.friendship (source_id, target_id, created_at) FROM stdin;
1	2	2020-12-08 22:37:56.803536+08
1	3	2020-12-08 22:37:56.803536+08
1	4	2020-12-08 22:37:56.803536+08
1	5	2020-12-08 22:37:56.803536+08
1	6	2020-12-08 22:37:56.803536+08
1	7	2020-12-08 22:37:56.803536+08
1	8	2020-12-08 22:37:56.803536+08
1	9	2020-12-08 22:37:56.803536+08
2	1	2020-12-08 22:37:56.803536+08
3	1	2020-12-08 22:37:56.803536+08
4	1	2020-12-08 22:37:56.803536+08
5	1	2020-12-08 22:37:56.803536+08
6	1	2020-12-08 22:37:56.803536+08
7	1	2020-12-08 22:37:56.803536+08
8	1	2020-12-08 22:37:56.803536+08
9	1	2020-12-08 22:37:56.803536+08
5	10	2020-12-08 22:37:56.803536+08
5	12	2020-12-08 22:37:56.803536+08
8	11	2020-12-08 22:37:56.803536+08
\.


--
-- Data for Name: property; Type: TABLE DATA; Schema: app_public; Owner: friensco
--

COPY app_public.property (id, user_id, help_attitude, is_closed, searchable, created_at, tags, urls, description, is_private) FROM stdin;
1	2	about_me	f	t	2020-12-08 22:37:56.803536+08	{mop,houseware,cleaning}	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}	supamop. 	f
2	2	about_me	f	t	2020-12-08 22:37:56.803536+08	{birthday}	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}	2000-06-19. 	f
3	2	about_me	f	t	2020-12-08 22:37:56.803536+08	{"relationship status"}	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}	in a relationship. 	f
4	2	about_me	f	t	2020-12-08 22:37:56.803536+08	{"number of children"}	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}	2. 	f
5	2	about_me	f	t	2020-12-08 22:37:56.803536+08	{occupation}	{https://picsum.photos/200/300.jpg,https://picsum.photos/300/200.jpg,https://picsum.photos/400/300.jpg}	Geologist. 	f
6	2	about_me	f	t	2020-12-08 22:37:56.803536+08	{"office software"}	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}	Excel. I think I know how to use Microsoft Excel very much. welcome discussion!	f
7	2	about_me	f	t	2020-12-08 22:37:56.803536+08	{}	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}	dinner. 	f
8	2	about_me	f	t	2020-12-08 22:37:56.803536+08	{family}	{https://picsum.photos/200/300.jpg,https://picsum.photos/300/200.jpg,https://picsum.photos/400/300.jpg}	taking care of kids. I dare not say myself an expert, but I really really research a lot for raising my kids. I'm happy to share my experience with you	f
9	4	about_me	f	t	2020-12-08 22:37:56.803536+08	{"office management","community management"}	{https://picsum.photos/200/300.jpg,https://picsum.photos/300/200.jpg,https://picsum.photos/400/300.jpg}	coworking space. 	f
10	4	about_me	f	t	2020-12-08 22:37:56.803536+08	{"relationship status"}	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}	single. 	f
11	4	about_me	f	t	2020-12-08 22:37:56.803536+08	{birthday}	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}	1980-11-18. 	f
12	4	about_me	f	t	2020-12-08 22:37:56.803536+08	{}	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}	China. I worked in China for 10 years.	f
13	4	call_for_help	f	t	2020-12-08 22:37:56.803536+08	{accommodation,roommate}	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}	rent in SF Bay Area. Private rooms, good Internet.	f
14	5	about_me	f	t	2020-12-08 22:37:56.803536+08	{vitamin,"dietary supplement",health}	{https://picsum.photos/200/300.jpg,https://picsum.photos/300/200.jpg,https://picsum.photos/400/300.jpg}	vitamin c. 	f
15	5	about_me	f	t	2020-12-08 22:37:56.803536+08	{birthday}	{https://picsum.photos/200/300.jpg,https://picsum.photos/300/200.jpg,https://picsum.photos/400/300.jpg}	1983-10-17. 	f
16	5	about_me	f	t	2020-12-08 22:37:56.803536+08	{accommodation,roommate}	{https://picsum.photos/200/300.jpg,https://picsum.photos/300/200.jpg,https://picsum.photos/400/300.jpg}	rent in Taipei. about 20k NTD per person. Kitchen and well functioning air conditioner required.	f
17	8	about_me	f	t	2020-12-08 22:37:56.803536+08	{health,attractiveness}	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}	fitness. 	f
18	8	about_me	f	t	2020-12-08 22:37:56.803536+08	{}	{https://picsum.photos/200/300.jpg,https://picsum.photos/300/200.jpg,https://picsum.photos/400/300.jpg}	hiking. 	f
19	8	about_me	f	t	2020-12-08 22:37:56.803536+08	{}	{https://picsum.photos/200/300.jpg,https://picsum.photos/300/200.jpg,https://picsum.photos/400/300.jpg}	welcome a dating. I'm open to make new friends or know more about old friends.	f
20	8	about_me	f	t	2020-12-08 22:37:56.803536+08	{birthday}	{https://picsum.photos/200/300.jpg,https://picsum.photos/300/200.jpg,https://picsum.photos/400/300.jpg}	1989-02-10. 	f
21	8	about_me	f	t	2020-12-08 22:37:56.803536+08	{gender}	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}	Male. 	f
22	8	about_me	f	t	2020-12-08 22:37:56.803536+08	{occupation}	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}	Photographer. 	f
23	8	about_me	f	t	2020-12-08 22:37:56.803536+08	{education}	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}	College. 	f
24	1	about_me	f	t	2020-12-08 22:37:56.803536+08	{book,novel,"Ayn Rand"}	{https://picsum.photos/200/300.jpg,https://picsum.photos/300/200.jpg,https://picsum.photos/400/300.jpg}	Atlas Shrugged. 	f
25	1	about_me	f	t	2020-12-08 22:37:56.803536+08	{book,philosophy,textbook,"Bertrand Russell"}	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}	The History OF Western Philosophy. 	f
26	1	about_me	f	t	2020-12-08 22:37:56.803536+08	{}	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}	meditation. I started to practice meditation since 10 years ago. Among the 10 years, I have accumulated about 3 years in which I do meditation as a habit.	f
27	6	about_me	f	t	2020-12-08 22:37:56.803536+08	{}	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}	cats. 3 cats in my house. I love cats. Any friend can ask me anything about cats	f
28	6	about_me	f	t	2020-12-08 22:37:56.803536+08	{"relationship status"}	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}	single. 	f
29	6	about_me	f	t	2020-12-08 22:37:56.803536+08	{}	{https://picsum.photos/200/300.jpg,https://picsum.photos/300/200.jpg,https://picsum.photos/400/300.jpg}	welcome a dating. His age or mental age should be greater than 40. Handsome is important to me. He has to be rich enough	f
30	6	about_me	f	t	2020-12-08 22:37:56.803536+08	{gender}	{https://picsum.photos/200/300.jpg,https://picsum.photos/300/200.jpg,https://picsum.photos/400/300.jpg}	Female. 	f
31	6	about_me	f	t	2020-12-08 22:37:56.803536+08	{occupation}	{https://picsum.photos/200/300.jpg,https://picsum.photos/300/200.jpg,https://picsum.photos/400/300.jpg}	Geologist. 	f
32	6	about_me	f	t	2020-12-08 22:37:56.803536+08	{occupation}	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}	Model. 	f
33	6	about_me	f	t	2020-12-08 22:37:56.803536+08	{}	{https://picsum.photos/200/300.jpg,https://picsum.photos/300/200.jpg,https://picsum.photos/400/300.jpg}	dance. 	f
34	9	about_me	f	t	2020-12-08 22:37:56.803536+08	{programming,"data science"}	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}	python. 	f
35	9	about_me	f	t	2020-12-08 22:37:56.803536+08	{birthday}	{https://picsum.photos/200/300.jpg,https://picsum.photos/300/200.jpg,https://picsum.photos/400/300.jpg}	1981-08-21. 	f
36	9	about_me	f	t	2020-12-08 22:37:56.803536+08	{}	{https://picsum.photos/200/300.jpg,https://picsum.photos/300/200.jpg,https://picsum.photos/400/300.jpg}	depression. My job in medicine gives me knowledge about depression. Welcome to ask me if you have questions.	f
37	9	call_for_help	f	t	2020-12-08 22:37:56.803536+08	{spirituality,health}	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}	meditation. 	f
38	9	call_for_help	f	t	2020-12-08 22:37:56.803536+08	{occupation}	{https://picsum.photos/200/300.jpg,https://picsum.photos/300/200.jpg,https://picsum.photos/400/300.jpg}	Accountant. 	f
39	1	call_for_help	f	t	2020-12-24 12:19:40.261238+08	{}	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}	girl's talk. 	f
40	1	call_for_help	f	t	2020-12-24 12:19:40.261238+08	{}	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}	welcome a dating. Athlete, humorous, outgoing, like sports	f
41	1	about_me	f	t	2020-12-24 12:19:40.261238+08	{}	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}	Angel Investor. 	f
42	1	call_for_help	f	t	2020-12-24 12:19:40.261238+08	{}	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}	welcome a dating. a sincere relationship. no game. living each other and support each other. I like and have a dog. Hope you also like dogs	f
43	1	about_me	f	t	2020-12-24 12:19:40.266315+08	{}	{https://picsum.photos/200/300.jpg,https://picsum.photos/300/200.jpg,https://picsum.photos/400/300.jpg}	depression. My job in medicine gives me knowledge about depression. Welcome to ask me if you have questions.	f
44	1	call_for_help	f	t	2020-12-24 12:19:40.266315+08	{"office management","community management"}	{https://picsum.photos/200/300.jpg,https://picsum.photos/300/200.jpg,https://picsum.photos/400/300.jpg}	coworking space. 	f
45	1	about_me	f	t	2020-12-24 12:19:40.266315+08	{occupation}	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}	Auditor. 	f
46	1	call_for_help	f	t	2020-12-24 12:19:40.266315+08	{birthday}	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}	1992-12-02. 	f
47	5	about_me	f	t	2020-12-24 12:19:40.266315+08	{}	{https://picsum.photos/200/300.jpg,https://picsum.photos/300/200.jpg,https://picsum.photos/400/300.jpg}	complaining boss. 	f
48	6	call_for_help	f	t	2020-12-24 12:19:40.266315+08	{"dietary supplement",health}	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}	fish oils. 	f
49	7	about_me	f	t	2020-12-24 12:19:40.266315+08	{}	{https://picsum.photos/200/300.jpg,https://picsum.photos/300/200.jpg,https://picsum.photos/400/300.jpg}	welcome a dating. I'm open to make new friends or know more about old friends.	f
50	7	call_for_help	f	t	2020-12-24 12:19:40.266315+08	{}	{https://picsum.photos/200/300.jpg,https://picsum.photos/300/200.jpg,https://picsum.photos/400/300.jpg}	dance. 	f
51	7	about_me	f	t	2020-12-24 12:19:40.266315+08	{}	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}	spirituality. 	f
52	8	call_for_help	f	t	2020-12-24 12:19:40.266315+08	{}	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}	China. I worked in China for 10 years.	f
53	9	about_me	f	t	2020-12-24 12:19:40.266315+08	{}	{https://picsum.photos/200/300.jpg,https://picsum.photos/300/200.jpg,https://picsum.photos/400/300.jpg}	welcome a dating. I prefer a relationship which could lead to marriage. My ideal type of date is an extravert, age under 35, and wants to have kids.	f
54	12	call_for_help	f	t	2020-12-24 12:19:40.266315+08	{education}	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}	College. 	f
55	12	about_me	f	t	2020-12-24 12:19:40.266315+08	{}	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}	Angel Investor. 	f
56	12	call_for_help	f	t	2020-12-24 12:19:40.266315+08	{book,philosophy,textbook,"Bertrand Russell"}	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}	The History OF Western Philosophy. 	f
57	12	about_me	f	t	2020-12-24 12:19:40.266315+08	{}	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}	cooking. 	f
58	14	call_for_help	f	t	2020-12-24 12:19:40.266315+08	{}	{https://picsum.photos/200/300.jpg,https://picsum.photos/300/200.jpg,https://picsum.photos/400/300.jpg}	camping. 	f
59	14	call_for_help	f	t	2020-12-24 12:19:40.266315+08	{birthday}	{https://picsum.photos/200/300.jpg,https://picsum.photos/300/200.jpg,https://picsum.photos/400/300.jpg}	1981-08-21. 	f
\.


--
-- Data for Name: property_group; Type: TABLE DATA; Schema: app_public; Owner: friensco
--

COPY app_public.property_group (property_id, virtual_user_id, created_at) FROM stdin;
39	1	2020-12-24 12:19:40.271044+08
40	1	2020-12-24 12:19:40.271044+08
41	1	2020-12-24 12:19:40.271044+08
39	7	2020-12-24 12:19:40.271044+08
40	7	2020-12-24 12:19:40.271044+08
41	7	2020-12-24 12:19:40.271044+08
42	6	2020-12-24 12:19:40.271044+08
43	2	2020-12-24 12:19:40.271044+08
44	3	2020-12-24 12:19:40.271044+08
45	3	2020-12-24 12:19:40.271044+08
46	4	2020-12-24 12:19:40.271044+08
47	5	2020-12-24 12:19:40.271044+08
48	6	2020-12-24 12:19:40.271044+08
49	7	2020-12-24 12:19:40.271044+08
50	8	2020-12-24 12:19:40.271044+08
51	9	2020-12-24 12:19:40.271044+08
52	10	2020-12-24 12:19:40.271044+08
53	11	2020-12-24 12:19:40.271044+08
54	12	2020-12-24 12:19:40.271044+08
55	13	2020-12-24 12:19:40.271044+08
56	14	2020-12-24 12:19:40.271044+08
57	15	2020-12-24 12:19:40.271044+08
58	16	2020-12-24 12:19:40.271044+08
59	17	2020-12-24 12:19:40.271044+08
48	18	2020-12-24 12:19:40.271044+08
48	19	2020-12-24 12:19:40.271044+08
50	20	2020-12-24 12:19:40.271044+08
49	20	2020-12-24 12:19:40.271044+08
12	20	2020-12-24 12:19:40.271044+08
7	20	2020-12-24 12:19:40.271044+08
43	20	2020-12-24 12:19:40.271044+08
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: app_public; Owner: friensco
--

COPY app_public.users (id, name, email, avatar_url, cellphone, created_at, updated_at, is_verified, featured_photo_urls) FROM stdin;
4	Cherish Beard	zwood@live.com	https://robohash.org/Vei2thei	\N	2020-12-08 22:37:56.803536+08	2020-12-08 22:37:56.803536+08	f	\N
5	Yusuf Blackwell	vertigo@yahoo.com	https://robohash.org/Chai6Que	\N	2020-12-08 22:37:56.803536+08	2020-12-08 22:37:56.803536+08	f	\N
6	Janessa Gonzalez	thrymm@yahoo.ca	https://robohash.org/oobu1Equ	\N	2020-12-08 22:37:56.803536+08	2020-12-08 22:37:56.803536+08	f	\N
7	Carolina Blevins	bmcmahon@verizon.net	https://robohash.org/Ieghai0g	\N	2020-12-08 22:37:56.803536+08	2020-12-08 22:37:56.803536+08	f	\N
8	Jordyn Curry	webteam@icloud.com	https://robohash.org/ahvius7G	\N	2020-12-08 22:37:56.803536+08	2020-12-08 22:37:56.803536+08	f	\N
9	Jovanny Anderson	granboul@mac.com	https://robohash.org/Naefoh5j	\N	2020-12-08 22:37:56.803536+08	2020-12-08 22:37:56.803536+08	f	\N
10	Cameron Jordan	jonadab@aol.com	https://robohash.org/ih0Eep0t	\N	2020-12-08 22:37:56.803536+08	2020-12-08 22:37:56.803536+08	f	\N
11	Jack Williamson	balchen@aol.com	https://robohash.org/li1GahNa	\N	2020-12-08 22:37:56.803536+08	2020-12-08 22:37:56.803536+08	f	\N
12	Osvaldo Knapp	rattenbt@live.com	https://robohash.org/Ni3nahng	\N	2020-12-08 22:37:56.803536+08	2020-12-08 22:37:56.803536+08	f	\N
13	Deja Cantu	sinkou@live.com	https://robohash.org/shei2Jai	\N	2020-12-08 22:37:56.803536+08	2020-12-08 22:37:56.803536+08	f	\N
14	Quinn Carter	markjugg@gmail.com	https://robohash.org/Iebie7Gi	\N	2020-12-08 22:37:56.803536+08	2020-12-08 22:37:56.803536+08	f	\N
15	Rudy Dixon	mugwump@optonline.net	https://robohash.org/eChaoth3	\N	2020-12-08 22:37:56.803536+08	2020-12-08 22:37:56.803536+08	f	\N
1	Shaka	scchen@shaka.today	https://robohash.org/IeBai5ei	\N	2020-12-08 22:37:56.803536+08	2020-12-08 22:37:56.803536+08	t	\N
2	Eveline	eveline@eveline.eveline	https://robohash.org/eiBie4au	\N	2020-12-08 22:37:56.803536+08	2020-12-08 22:37:56.803536+08	f	{https://picsum.photos/id/1027/284/427.jpg,https://picsum.photos/id/1011/547/364.jpg,https://picsum.photos/id/1006/300/200.jpg}
3	stone man	stone@stoneman.tw	https://robohash.org/chahL8eb	\N	2020-12-08 22:37:56.803536+08	2020-12-08 22:37:56.803536+08	f	{https://picsum.photos/id/1005/576/384.jpg,https://picsum.photos/id/1009/500/750.jpg,https://picsum.photos/id/103/259/193.jpg,https://picsum.photos/id/1042/345/518.jpg,https://picsum.photos/id/1050/600/400.jpg}
\.


--
-- Data for Name: virtual_user; Type: TABLE DATA; Schema: app_public; Owner: friensco
--

COPY app_public.virtual_user (id, user_id, target_user_id, name, display_name, can_do_referral, created_at, featured_photo_urls) FROM stdin;
1	1	2	Carmen Phelps	Dominique Conway	f	2020-12-24 12:19:40.248434+08	\N
2	1	2	Jimmie Padilla	Gene Mccullough	f	2020-12-24 12:19:40.248434+08	\N
3	2	2	Essie Sexton	Milo Mccarty	f	2020-12-24 12:19:40.248434+08	\N
4	2	1	Silas Bowman	Hallie Rios	f	2020-12-24 12:19:40.248434+08	\N
5	1	1	Doreen Dillon	Doreen Bowman	f	2020-12-24 12:19:40.248434+08	\N
6	1	3	Cody Harmon	Tamera Greene	t	2020-12-24 12:19:40.258015+08	\N
7	1	4	Julia Maynard	Heidi Klein	t	2020-12-24 12:19:40.258015+08	\N
9	3	1	Roberto Cox	Miriam Kennedy	t	2020-12-24 12:19:40.258015+08	\N
10	1	1	Jess Stein	Israel Moody	t	2020-12-24 12:19:40.258015+08	\N
11	1	\N	Norman Everett	Roxie Cantrell	f	2020-12-24 12:19:40.259628+08	\N
12	1	\N	Mari Finley	Hallie Todd	f	2020-12-24 12:19:40.259628+08	\N
13	1	\N	Richard Torres	Wilford Sweet	f	2020-12-24 12:19:40.259628+08	\N
14	2	\N	Chester Morales	Pam Frost	f	2020-12-24 12:19:40.259628+08	\N
15	2	\N	Serena Cantu	Geoffrey Mcmahon	f	2020-12-24 12:19:40.259628+08	\N
16	1	\N	Brad Chang	Lina Mejia	t	2020-12-24 12:19:40.260463+08	\N
17	1	\N	Diann Phillips	Dee Simpson	t	2020-12-24 12:19:40.260463+08	\N
19	2	\N	Candice Craft	Rod Sykes	t	2020-12-24 12:19:40.260463+08	\N
20	3	\N	Karina Pugh	Terrence Gay	t	2020-12-24 12:19:40.260463+08	\N
8	3	3	Harley Daniels	Myron Mccarthy	t	2020-12-24 12:19:40.258015+08	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}
18	2	\N	Alexandra Puckett	Fredric Macdonald	t	2020-12-24 12:19:40.260463+08	{https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg,https://picsum.photos/300/200.jpg}
\.


--
-- Name: direct_message_id_seq; Type: SEQUENCE SET; Schema: app_public; Owner: friensco
--

SELECT pg_catalog.setval('app_public.direct_message_id_seq', 1, false);


--
-- Name: help_id_seq; Type: SEQUENCE SET; Schema: app_public; Owner: friensco
--

SELECT pg_catalog.setval('app_public.help_id_seq', 59, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: app_public; Owner: friensco
--

SELECT pg_catalog.setval('app_public.users_id_seq', 15, true);


--
-- Name: virtual_user_id_seq; Type: SEQUENCE SET; Schema: app_public; Owner: friensco
--

SELECT pg_catalog.setval('app_public.virtual_user_id_seq', 20, true);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: app_private; Owner: friensco
--

ALTER TABLE ONLY app_private.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: direct_message direct_message_pkey; Type: CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.direct_message
    ADD CONSTRAINT direct_message_pkey PRIMARY KEY (id);


--
-- Name: friendship friendship_pkey; Type: CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.friendship
    ADD CONSTRAINT friendship_pkey PRIMARY KEY (source_id, target_id);


--
-- Name: property_group property_group_pkey; Type: CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.property_group
    ADD CONSTRAINT property_group_pkey PRIMARY KEY (property_id, virtual_user_id);


--
-- Name: property property_pkey; Type: CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.property
    ADD CONSTRAINT property_pkey PRIMARY KEY (id);


--
-- Name: users users_avatar_url_key; Type: CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.users
    ADD CONSTRAINT users_avatar_url_key UNIQUE (avatar_url);


--
-- Name: users users_cellphone_key; Type: CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.users
    ADD CONSTRAINT users_cellphone_key UNIQUE (cellphone);


--
-- Name: users users_email_key; Type: CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.users
    ADD CONSTRAINT users_email_key UNIQUE (email);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: virtual_user virtual_user_pkey; Type: CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.virtual_user
    ADD CONSTRAINT virtual_user_pkey PRIMARY KEY (id);


--
-- Name: virtual_user virtual_user_user_id_display_name_key; Type: CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.virtual_user
    ADD CONSTRAINT virtual_user_user_id_display_name_key UNIQUE (user_id, display_name);


--
-- Name: virtual_user virtual_user_user_id_name_key; Type: CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.virtual_user
    ADD CONSTRAINT virtual_user_user_id_name_key UNIQUE (user_id, name);


--
-- Name: direct_message_created_at_idx; Type: INDEX; Schema: app_public; Owner: friensco
--

CREATE INDEX direct_message_created_at_idx ON app_public.direct_message USING btree (created_at);


--
-- Name: direct_message_target_user_id_idx; Type: INDEX; Schema: app_public; Owner: friensco
--

CREATE INDEX direct_message_target_user_id_idx ON app_public.direct_message USING btree (target_user_id);


--
-- Name: direct_message_user_id_idx; Type: INDEX; Schema: app_public; Owner: friensco
--

CREATE INDEX direct_message_user_id_idx ON app_public.direct_message USING btree (user_id);


--
-- Name: friendship_target_id_idx; Type: INDEX; Schema: app_public; Owner: friensco
--

CREATE INDEX friendship_target_id_idx ON app_public.friendship USING btree (target_id);


--
-- Name: property_created_at_idx; Type: INDEX; Schema: app_public; Owner: friensco
--

CREATE INDEX property_created_at_idx ON app_public.property USING btree (created_at);


--
-- Name: property_group_virtual_user_id_idx; Type: INDEX; Schema: app_public; Owner: friensco
--

CREATE INDEX property_group_virtual_user_id_idx ON app_public.property_group USING btree (virtual_user_id);


--
-- Name: property_help_attitude_idx; Type: INDEX; Schema: app_public; Owner: friensco
--

CREATE INDEX property_help_attitude_idx ON app_public.property USING btree (help_attitude);


--
-- Name: property_is_private_idx; Type: INDEX; Schema: app_public; Owner: friensco
--

CREATE INDEX property_is_private_idx ON app_public.property USING btree (is_private);


--
-- Name: property_user_id_idx; Type: INDEX; Schema: app_public; Owner: friensco
--

CREATE INDEX property_user_id_idx ON app_public.property USING btree (user_id);


--
-- Name: virtual_user_target_user_id_idx; Type: INDEX; Schema: app_public; Owner: friensco
--

CREATE INDEX virtual_user_target_user_id_idx ON app_public.virtual_user USING btree (target_user_id);


--
-- Name: users users_id_fkey; Type: FK CONSTRAINT; Schema: app_private; Owner: friensco
--

ALTER TABLE ONLY app_private.users
    ADD CONSTRAINT users_id_fkey FOREIGN KEY (id) REFERENCES app_public.users(id) ON DELETE CASCADE;


--
-- Name: direct_message direct_message_target_user_id_fkey; Type: FK CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.direct_message
    ADD CONSTRAINT direct_message_target_user_id_fkey FOREIGN KEY (target_user_id) REFERENCES app_public.users(id);


--
-- Name: direct_message direct_message_user_id_fkey; Type: FK CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.direct_message
    ADD CONSTRAINT direct_message_user_id_fkey FOREIGN KEY (user_id) REFERENCES app_public.users(id);


--
-- Name: friendship friendship_source_id_fkey; Type: FK CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.friendship
    ADD CONSTRAINT friendship_source_id_fkey FOREIGN KEY (source_id) REFERENCES app_public.users(id);


--
-- Name: friendship friendship_target_id_fkey; Type: FK CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.friendship
    ADD CONSTRAINT friendship_target_id_fkey FOREIGN KEY (target_id) REFERENCES app_public.users(id);


--
-- Name: property_group help_group_help_id_fkey; Type: FK CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.property_group
    ADD CONSTRAINT help_group_help_id_fkey FOREIGN KEY (property_id) REFERENCES app_public.property(id);


--
-- Name: property_group help_group_virtual_user_id_fkey; Type: FK CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.property_group
    ADD CONSTRAINT help_group_virtual_user_id_fkey FOREIGN KEY (virtual_user_id) REFERENCES app_public.virtual_user(id);


--
-- Name: property help_user_id_fkey; Type: FK CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.property
    ADD CONSTRAINT help_user_id_fkey FOREIGN KEY (user_id) REFERENCES app_public.users(id);


--
-- Name: virtual_user virtual_user_target_user_id_fkey; Type: FK CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.virtual_user
    ADD CONSTRAINT virtual_user_target_user_id_fkey FOREIGN KEY (target_user_id) REFERENCES app_public.users(id);


--
-- Name: virtual_user virtual_user_user_id_fkey; Type: FK CONSTRAINT; Schema: app_public; Owner: friensco
--

ALTER TABLE ONLY app_public.virtual_user
    ADD CONSTRAINT virtual_user_user_id_fkey FOREIGN KEY (user_id) REFERENCES app_public.users(id);


--
-- Name: SCHEMA app_private; Type: ACL; Schema: -; Owner: friensco
--

GRANT USAGE ON SCHEMA app_private TO friensco_visitor;


--
-- Name: SCHEMA app_public; Type: ACL; Schema: -; Owner: friensco
--

GRANT USAGE ON SCHEMA app_public TO friensco_user;
GRANT USAGE ON SCHEMA app_public TO friensco_visitor;


--
-- Name: TABLE users; Type: ACL; Schema: app_public; Owner: friensco
--

GRANT ALL ON TABLE app_public.users TO friensco_user;


--
-- Name: FUNCTION authenticate(email text, password text); Type: ACL; Schema: app_public; Owner: friensco
--

GRANT ALL ON FUNCTION app_public.authenticate(email text, password text) TO friensco_user;
GRANT ALL ON FUNCTION app_public.authenticate(email text, password text) TO friensco_visitor;


--
-- Name: FUNCTION get_current_user_id(); Type: ACL; Schema: app_public; Owner: friensco
--

GRANT ALL ON FUNCTION app_public.get_current_user_id() TO friensco_user;


--
-- Name: TABLE property; Type: ACL; Schema: app_public; Owner: friensco
--

GRANT ALL ON TABLE app_public.property TO friensco_user;


--
-- Name: TABLE virtual_user; Type: ACL; Schema: app_public; Owner: friensco
--

GRANT ALL ON TABLE app_public.virtual_user TO friensco_user;


--
-- Name: TABLE direct_message; Type: ACL; Schema: app_public; Owner: friensco
--

GRANT ALL ON TABLE app_public.direct_message TO friensco_user;


--
-- Name: TABLE friendship; Type: ACL; Schema: app_public; Owner: friensco
--

GRANT ALL ON TABLE app_public.friendship TO friensco_user;


--
-- Name: TABLE property_group; Type: ACL; Schema: app_public; Owner: friensco
--

GRANT ALL ON TABLE app_public.property_group TO friensco_user;


--
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: app_public; Owner: friensco
--

ALTER DEFAULT PRIVILEGES FOR ROLE friensco IN SCHEMA app_public REVOKE ALL ON TABLES  FROM friensco;
ALTER DEFAULT PRIVILEGES FOR ROLE friensco IN SCHEMA app_public GRANT ALL ON TABLES  TO friensco_user;


--
-- PostgreSQL database dump complete
--


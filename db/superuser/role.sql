ALTER ROLE friensco REPLICATION;

CREATE ROLE friensco_visitor NOLOGIN;

CREATE ROLE friensco_user NOLOGIN;

GRANT friensco_user TO friensco;

GRANT friensco_visitor TO friensco;


CREATE TYPE app_public.address_type AS ENUM (
  'cellphone',
  'fax',
  'fixedline_telephone',
  'email',
  'mail',
  'real_estate',
  'others'
);

CREATE TYPE app_public.jwt_token AS (
  ROLE text,
  expire integer,
  user_id integer,
  username varchar
);

CREATE TYPE app_public.help_attitude AS ENUM (
  'looking_for_people_who',
  'is_a_person_who'
);


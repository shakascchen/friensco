CREATE FUNCTION app_public.signup (email VARCHAR, NAME VARCHAR, PASSWORD TEXT)
  RETURNS app_public.jwt_token
  AS $$
DECLARE
  user_password_salted TEXT = crypt(PASSWORD, gen_salt('bf'));
  user_email VARCHAR;
  user_name VARCHAR;
  user_id INT;
BEGIN
  WITH new_user AS (
INSERT INTO app_public.users (email, name)
      VALUES (email, NAME)
    RETURNING
      app_public.users.ID, app_public.users.NAME, app_public.users.email)
    SELECT
      new_user.ID,
      new_user.NAME,
      new_user.email INTO user_id,
      user_name,
      user_email
    FROM
      new_user;
  -- TODO refine transaction management
  INSERT INTO app_private.users (ID, password_salted)
    VALUES (user_id, user_password_salted);
  -- TODO refine transaction management
  INSERT INTO app_public.contact (user_id, target_user_id, email, display_name, avatar_url)
    VALUES (user_id, user_id, user_email, user_name, 'https://robohash.org/' || SUBSTRING(md5(now()::TEXT), 1, 8));
  IF user_email IS NOT NULL AND user_name IS NOT NULL AND user_id IS NOT NULL THEN
    RETURN ('friensco_user',
      extract(epoch FROM now() + interval '7 days'),
      user_id,
      user_name)::app_public.jwt_token;
  ELSE
    RETURN NULL;
  END IF;
END;
$$
LANGUAGE plpgsql
VOLATILE STRICT
SECURITY DEFINER;

CREATE FUNCTION app_public.authenticate (email text, PASSWORD text)
  RETURNS app_public.jwt_token
  AS $$
DECLARE
  user_password_salted TEXT;
  user_email VARCHAR;
  user_name VARCHAR;
  user_id INT;
BEGIN
  SELECT
    app_public.users.ID,
    app_public.users.email,
    app_public.users.NAME,
    app_private.users.password_salted INTO user_id,
    user_email,
    user_name,
    user_password_salted
  FROM
    app_private.users
    INNER JOIN app_public.users ON app_private.users.ID = app_public.users.ID
  WHERE
    app_public.users.email = authenticate.email;
  IF user_password_salted = crypt(PASSWORD, user_password_salted) THEN
    RETURN ('friensco_user',
      extract(epoch FROM now() + interval '7 days'),
      user_id,
      user_name)::app_public.jwt_token;
  ELSE
    RETURN NULL;
  END IF;
END;
$$
LANGUAGE plpgsql
STRICT
SECURITY DEFINER;

CREATE FUNCTION app_public.get_current_user_id ()
  RETURNS INT
  AS $$
  SELECT
    current_setting('jwt.claims.user_id', TRUE)::int;

$$
LANGUAGE sql
STABLE;


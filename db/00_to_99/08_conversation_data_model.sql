SET ROLE friensco;

CREATE TABLE app_public.direct_message (
  id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  user_id INT NOT NULL REFERENCES app_public.users,
  target_user_id INT NOT NULL REFERENCES app_public.users,
  TEXT TEXT NOT NULL,
  image_url VARCHAR,
  video_url VARCHAR,
  audio_url VARCHAR,
  created_at TIMESTAMPTZ NOT NULL DEFAULT now()
);

CREATE INDEX ON app_public.direct_message (user_id);

CREATE INDEX ON app_public.direct_message (target_user_id);


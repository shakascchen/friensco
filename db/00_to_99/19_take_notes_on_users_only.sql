ALTER TABLE app_public.property
  ADD COLUMN title VARCHAR NOT NULL DEFAULT '',
  DROP COLUMN searchable,
  DROP COLUMN help_attitude;

UPDATE
  app_public.property
SET
  is_private = TRUE;

ALTER TABLE app_public.property
  ALTER COLUMN is_private SET DEFAULT TRUE;

ALTER TABLE app_public.virtual_user
  DROP COLUMN NAME,
  DROP CONSTRAINT virtual_user_user_id_display_name_key;

CREATE INDEX ON app_public.virtual_user (user_id);


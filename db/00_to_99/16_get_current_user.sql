SET ROLE friensco;

CREATE FUNCTION app_public.get_current_user ()
  RETURNS app_public.users
  AS $$
  SELECT
    *
  FROM
    app_public.users
  WHERE
    ID = current_setting('jwt.claims.user_id', TRUE)::INT
  LIMIT 1;

$$
LANGUAGE sql
STABLE;


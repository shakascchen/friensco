ALTER TABLE app_public.help
  DROP COLUMN target_user_id,
  DROP can_do_referral;

SET ROLE friensco;

CREATE TABLE app_public.virtual_user (
  id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  user_id INT NOT NULL REFERENCES app_public.users,
  target_user_id INT REFERENCES app_public.users,
  NAME VARCHAR NOT NULL,
  display_name VARCHAR NOT NULL, -- front end can provide random name
  can_do_referral BOOLEAN NOT NULL DEFAULT FALSE,
  UNIQUE (user_id, NAME),
  UNIQUE (user_id, display_name),
  created_at TIMESTAMPTZ NOT NULL DEFAULT now()
);

CREATE TABLE app_public.help_group (
  help_id INT REFERENCES app_public.help,
  virtual_user_id INT REFERENCES app_public.virtual_user,
  PRIMARY KEY (help_id, virtual_user_id),
  created_at TIMESTAMPTZ NOT NULL DEFAULT now()
);

CREATE FUNCTION app_public.help_is_private_note (help_record app_public.help)
  RETURNS BOOLEAN
  AS $$
  SELECT
    (0 < (
        SELECT
          COUNT(*)
        FROM
          app_public.help_group
        WHERE
          help_record.ID = app_public.help_group.help_id))
$$
LANGUAGE SQL
STABLE;


ALTER TABLE app_public.users
  ADD COLUMN featured_photo_urls VARCHAR[];

ALTER TABLE app_public.virtual_user
  ADD COLUMN featured_photo_urls VARCHAR[];

ALTER TYPE app_public.help_attitude RENAME VALUE 'looking_for_people_who' TO 'call_for_help';

ALTER TYPE app_public.help_attitude RENAME VALUE 'is_a_person_who' TO 'about_me';

ALTER TABLE app_public.property_node RENAME TO property;

ALTER TABLE app_public.property RENAME COLUMN TYPES TO tags;

ALTER TABLE app_public.property
  ALTER COLUMN urls TYPE VARCHAR[]
  USING ARRAY[]::VARCHAR[];

UPDATE
  app_public.property
SET
  description = NAME || '. ' || description;

ALTER TABLE app_public.property
  DROP COLUMN NAME;


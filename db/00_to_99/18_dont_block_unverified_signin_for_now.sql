CREATE OR REPLACE FUNCTION app_public.authenticate (email text, PASSWORD text)
  RETURNS app_public.jwt_token
  AS $$
DECLARE
  user_password_salted TEXT;
  user_email VARCHAR;
  user_name VARCHAR;
  user_id INT;
BEGIN
  SELECT
    app_public.users.ID,
    app_public.users.email,
    app_public.users.NAME,
    app_private.users.password_salted INTO user_id,
    user_email,
    user_name,
    user_password_salted
  FROM
    app_private.users
    INNER JOIN app_public.users ON app_private.users.ID = app_public.users.ID
  WHERE
    app_public.users.email = authenticate.email;
  IF user_password_salted = crypt(PASSWORD, user_password_salted) THEN
    RETURN ('friensco_user',
      extract(epoch FROM now() + interval '7 days'),
      user_id,
      user_name)::app_public.jwt_token;
  ELSE
    RETURN NULL;
  END IF;
END;
$$
LANGUAGE plpgsql
STRICT
SECURITY DEFINER;


DELETE FROM app_public.property_group;

DELETE FROM app_public.virtual_user;

ALTER TABLE app_public.virtual_user
  ADD COLUMN imported_id VARCHAR NOT NULL,
  ADD COLUMN imported_data jsonb;

ALTER TABLE app_public.virtual_user
  ADD CONSTRAINT virtual_user_id_imported_id_unique UNIQUE (user_id, imported_id);

SET ROLE friensco;

CREATE FUNCTION app_public.sync_virtual_user (contact_data jsonb)
  RETURNS SETOF app_public.virtual_user
  AS $$
DECLARE
  current_user_id INT = current_setting('jwt.claims.user_id', TRUE)::INT;
BEGIN
  RETURN query INSERT INTO app_public.virtual_user (user_id, display_name, imported_id, imported_data)
  SELECT
    current_user_id AS user_id,
    VALUE::jsonb ->> 'name' AS display_name,
    VALUE::jsonb ->> 'id' AS imported_id,
    VALUE AS imported_data
  FROM
    jsonb_array_elements(contact_data)
  ON CONFLICT (user_id,
    imported_id)
    DO UPDATE SET
      display_name = excluded.display_name,
      imported_data = excluded.imported_data
    RETURNING
      *;
END;
$$
LANGUAGE plpgsql
STRICT VOLATILE;


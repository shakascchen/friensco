ALTER TABLE app_public.help
  ADD COLUMN tags VARCHAR[] NOT NULL DEFAULT ARRAY[]::VARCHAR[],
  ADD COLUMN urls VARCHAR[] NOT NULL DEFAULT ARRAY[]::VARCHAR[],
  ADD COLUMN description TEXT NOT NULL DEFAULT '',
  ADD COLUMN is_private BOOLEAN NOT NULL DEFAULT FALSE;

UPDATE
  app_public.help
SET
  tags = p.tags,
  urls = p.urls,
  description = p.description,
  is_private = p.is_private
FROM (
  SELECT
    tags,
    urls,
    description,
    is_private,
    id
  FROM
    app_public.property) AS P
WHERE
  help.property_id = p.ID;

DROP TABLE app_public.property CASCADE;

ALTER TABLE app_public.help RENAME TO property;

ALTER TABLE app_public.property
  DROP COLUMN property_id;

ALTER TABLE app_public.help_group RENAME TO property_group;

ALTER TABLE app_public.property_group RENAME COLUMN help_id TO property_id;

DROP FUNCTION app_public.help_is_private_note;

CREATE FUNCTION app_public.property_is_private_note (property_record app_public.property)
  RETURNS BOOLEAN
  AS $$
  SELECT
    (0 < (
        SELECT
          COUNT(*)
        FROM
          app_public.property_group
        WHERE
          property_record.ID = app_public.property_group.property_id))
$$
LANGUAGE SQL
STABLE;

ALTER FUNCTION app_public.property_is_private_note OWNER TO friensco;

ALTER INDEX app_public.help_group_pkey RENAME TO property_group_pkey;

ALTER INDEX app_public.help_pkey RENAME TO property_pkey;

ALTER INDEX app_public.help_group_virtual_user_id_idx RENAME TO property_group_virtual_user_id_idx;

ALTER INDEX app_public.help_help_attitude_idx RENAME TO property_help_attitude_idx;

ALTER INDEX app_public.help_user_id_idx RENAME TO property_user_id_idx;


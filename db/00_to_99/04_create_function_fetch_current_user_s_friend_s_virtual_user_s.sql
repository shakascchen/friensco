SET ROLE friensco;

CREATE FUNCTION app_public.virtual_users_who_friends_can_do_referral_to_current_user ()
  RETURNS SETOF app_public.virtual_user
  AS $$
DECLARE
  current_user_id INT := current_setting('jwt.claims.user_id', TRUE)::INT;
BEGIN
  RETURN query
  SELECT
    *
  FROM
    app_public.virtual_user
  WHERE
    app_public.virtual_user.user_id IN (
      SELECT
        app_public.friendship.target_id
      FROM
        app_public.friendship
      WHERE
        app_public.friendship.source_id = current_user_id)
    AND app_public.virtual_user.can_do_referral = TRUE;
END;
$$
LANGUAGE plpgsql
STABLE;


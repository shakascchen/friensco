ALTER FUNCTION app_public.property_is_private_note RENAME TO property_is_owned_by_virtual_user;

CREATE INDEX ON app_public.property (is_private);

CREATE INDEX ON app_public.property (created_at);


INSERT INTO app_public.object_of_help (NAME)
  VALUES (
    /*1*/
    'profile'), (
    /*2*/
    'provide item'), (
    /*3*/
    /*Because hosting an event requires much more involvement than attendant */
    'host activity'), (
    /*4*/
    'attend activity'), (
    /*5*/
    'know/experience'), (
    /*6*/
    'want pay together');


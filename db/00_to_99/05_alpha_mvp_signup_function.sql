SET ROLE friensco;

ALTER DEFAULT PRIVILEGES IN SCHEMA app_public GRANT ALL ON TABLES TO friensco_user;

ALTER TABLE app_public.users
  ADD COLUMN is_verified BOOLEAN NOT NULL DEFAULT FALSE,
  ADD COLUMN who_you_are TEXT DEFAULT '';

ALTER TABLE app_public.users
  ALTER COLUMN who_you_are SET NOT NULL,
  ALTER COLUMN who_you_are DROP DEFAULT;

DROP FUNCTION app_public.signup;

CREATE FUNCTION app_public.signup (email VARCHAR, NAME VARCHAR, PASSWORD TEXT, who_you_are TEXT)
  RETURNS boolean
  AS $$
DECLARE
  user_password_salted TEXT = crypt(PASSWORD, gen_salt('bf'));
  user_email VARCHAR;
  user_name VARCHAR;
  user_id INT;
BEGIN
  WITH new_user AS (
INSERT INTO app_public.users (email, NAME, avatar_url, who_you_are)
      VALUES (email, NAME, 'https://robohash.org/' || SUBSTRING(md5(now()::TEXT), 1, 8), who_you_are)
    RETURNING
      app_public.users.ID, app_public.users.NAME, app_public.users.email)
    SELECT
      new_user.ID,
      new_user.NAME,
      new_user.email INTO user_id,
      user_name,
      user_email
    FROM
      new_user;
  -- TODO refine transaction management
  INSERT INTO app_private.users (ID, password_salted)
    VALUES (user_id, user_password_salted);
  -- TODO refine transaction management
  IF user_email IS NOT NULL AND user_name IS NOT NULL AND user_id IS NOT NULL THEN
    RETURN TRUE;
  ELSE
    RETURN FALSE;
  END IF;
  RETURN FALSE;
END;
$$
LANGUAGE plpgsql
VOLATILE STRICT
SECURITY DEFINER;

CREATE FUNCTION app_hidden.manually_set_verified (user_id INT)
  RETURNS app_public.users
  AS $$
BEGIN;

UPDATE
  app_public.users
SET
  is_verified = TRUE
WHERE
  app_public.users.ID = user_id;

INSERT INTO app_public.friendship (source_id, target_id)
  VALUES (1, user_id), (user_id, 1);

COMMIT;

SELECT
  *
FROM
  app_public.users
WHERE
  app_public.users.ID = user_id
$$
LANGUAGE SQL
VOLATILE STRICT;


-- By default (without BEGIN), PostgreSQL executes transactions in “autocommit” mode, that is, each statement is executed in its own transaction and a commit is implicitly performed at the end of the statement (if execution was successful, otherwise a rollback is done).  https://www.postgresql.org/docs/12/sql-begin.html
ALTER TABLE app_public.users
  DROP COLUMN who_you_are CASCADE;

DROP TABLE app_hidden.alpha_user;

DROP FUNCTION app_public.signup;

SET ROLE friensco;

CREATE FUNCTION app_public.signup (email VARCHAR, NAME VARCHAR, PASSWORD TEXT)
  RETURNS app_public.jwt_token
  AS $$
DECLARE
  user_password_salted TEXT = crypt(PASSWORD, gen_salt('bf'));
  user_email VARCHAR;
  user_name VARCHAR;
  user_id INT;
  alpha_user_email varchar;
BEGIN
  WITH new_user AS (
INSERT INTO app_public.users (email, NAME, avatar_url)
      VALUES (email, NAME, 'https://robohash.org/' || SUBSTRING(md5(now()::TEXT), 1, 8))
    RETURNING
      app_public.users.ID, app_public.users.NAME, app_public.users.email)
    SELECT
      new_user.ID,
      new_user.NAME,
      new_user.email INTO user_id,
      user_name,
      user_email
    FROM
      new_user;
  INSERT INTO app_private.users (ID, password_salted)
    VALUES (user_id, user_password_salted);
  IF (user_email,
    user_name,
    user_id) IS NOT NULL THEN
    INSERT INTO app_public.friendship (source_id, target_id)
      VALUES (1, user_id), (user_id, 1);
    RETURN ('friensco_user',
      extract(epoch FROM now() + interval '7 days'),
      user_id,
      user_name)::app_public.jwt_token;
  END IF;
  RETURN NULL;
END;
$$
LANGUAGE plpgsql
VOLATILE STRICT
SECURITY DEFINER;


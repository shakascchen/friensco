CREATE OR REPLACE FUNCTION app_hidden.manually_set_verified (user_id INT)
  RETURNS app_public.users
  AS $$
  UPDATE
    app_public.users
  SET
    is_verified = TRUE
  WHERE
    app_public.users.ID = user_id;

INSERT INTO app_public.friendship (source_id, target_id)
  VALUES (1, user_id), (user_id, 1);

SELECT
  *
FROM
  app_public.users
WHERE
  app_public.users.ID = user_id
$$
LANGUAGE SQL
VOLATILE STRICT;


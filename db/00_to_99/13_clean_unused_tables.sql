DROP TABLE app_public.object_of_help CASCADE;

ALTER TABLE app_public.help
  DROP COLUMN object_of_help_id CASCADE;

CREATE INDEX ON app_public.help (help_attitude);

DROP TABLE app_public.lazy_question;

DROP TABLE app_public.contact_address;


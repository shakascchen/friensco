CREATE INDEX ON app_public.contact_address (user_id);

CREATE INDEX ON app_public.contact_address (target_user_id);

CREATE INDEX ON app_public.help (user_id);

CREATE INDEX ON app_public.help (target_user_id);

CREATE INDEX ON app_public.help (help_attitude, object_of_help_id, property_id);

CREATE INDEX ON app_public.help (object_of_help_id);

CREATE INDEX ON app_public.help (property_id);

CREATE INDEX ON app_public.help (is_private_note);

CREATE INDEX ON app_public.lazy_question (user_id);

CREATE INDEX ON app_public.lazy_question (target_user_id);


/*
Disabled 'read' permission for constraint "friendship_target_id_fkey" on table "app_public"."friendship" because it isn't indexed. For more information see https://graphile.org/postgraphile/best-practices/ To fix, perform

 CREATE INDEX ON "app_public"."friendship"("target_id"); */
CREATE INDEX ON app_public.friendship (target_id);


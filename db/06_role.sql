-- friensco_user: after login
GRANT ALL ON ALL TABLES IN SCHEMA app_public TO friensco_user;

GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA app_public TO friensco_user;

GRANT USAGE ON SCHEMA app_public TO friensco_user;

-- friensco_visitor: before login
GRANT EXECUTE ON FUNCTION app_public.authenticate TO
GROUP friensco_visitor;

GRANT EXECUTE ON FUNCTION app_public.signup TO
GROUP friensco_visitor;

GRANT USAGE ON SCHEMA app_public TO friensco_visitor;

-- TODO: resolve potential security problems
GRANT USAGE ON SCHEMA app_private TO friensco_visitor;


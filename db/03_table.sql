CREATE TABLE app_public.users (
  id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  name VARCHAR NOT NULL,
  email VARCHAR UNIQUE NOT NULL,
  avatar_url VARCHAR UNIQUE NOT NULL,
  cellphone VARCHAR UNIQUE,
  created_at TIMESTAMPTZ NOT NULL DEFAULT now(),
  updated_at TIMESTAMPTZ NOT NULL DEFAULT now()
);

CREATE TABLE app_private.users (
  id INT PRIMARY KEY REFERENCES app_public.users ON DELETE CASCADE,
  updated_at TIMESTAMPTZ NOT NULL DEFAULT now(),
  password_salted TEXT NOT NULL
);

CREATE TABLE app_public.contact_address (
  TYPE app_public.address_type NOT NULL,
  usage VARCHAR,
  address VARCHAR NOT NULL,
  user_id INT NOT NULL REFERENCES app_public.users,
  target_user_id INT NOT NULL REFERENCES app_public.users,
  is_verified BOOLEAN NOT NULL DEFAULT FALSE,
  is_user_self_info BOOLEAN GENERATED ALWAYS AS (user_id = target_user_id) stored
);

-- two users are friends only if both directions are established
CREATE TABLE app_public.friendship (
  source_id INT NOT NULL REFERENCES app_public.users,
  target_id INT NOT NULL REFERENCES app_public.users,
  PRIMARY KEY (source_id, target_id),
  CHECK (source_id != target_id),
  created_at TIMESTAMPTZ NOT NULL DEFAULT now()
);

CREATE TABLE app_public.property_node (
  id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  NAME VARCHAR NOT NULL,
  types VARCHAR[] NOT NULL DEFAULT ARRAY[] ::VARCHAR[],
  urls VARCHAR NOT NULL DEFAULT ARRAY[] ::VARCHAR[],
  description TEXT NOT NULL DEFAULT '',
  is_private BOOLEAN NOT NULL DEFAULT FALSE,
  UNIQUE (NAME, types, urls, description),
  created_at TIMESTAMPTZ NOT NULL DEFAULT now()
);

CREATE TABLE app_public.object_of_help (
  id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  NAME VARCHAR NOT NULL,
  created_at TIMESTAMPTZ NOT NULL DEFAULT now()
);

CREATE TABLE app_public.lazy_question (
  id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  user_id INT NOT NULL REFERENCES app_public.users,
  target_user_id INT REFERENCES app_public.users,
  question TEXT NOT NULL,
  is_closed BOOLEAN NOT NULL DEFAULT FALSE,
  searchable BOOLEAN NOT NULL DEFAULT TRUE,
  created_at TIMESTAMPTZ NOT NULL DEFAULT now(),
  is_private_note BOOLEAN GENERATED ALWAYS AS (target_user_id IS NOT NULL) stored
);

CREATE TABLE app_public.help (
  id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  user_id INT NOT NULL REFERENCES app_public.users,
  -- should limit subject_property string length in frontend to avoid problems like putting questions as property.name -> difficult to do match .
  target_user_id INT REFERENCES app_public.users,
  help_attitude app_public.help_attitude NOT NULL,
  object_of_help_id INT NOT NULL REFERENCES app_public.object_of_help,
  property_id INT NOT NULL REFERENCES app_public.property_node,
  is_closed BOOLEAN NOT NULL DEFAULT FALSE,
  searchable BOOLEAN NOT NULL DEFAULT TRUE,
  created_at TIMESTAMPTZ NOT NULL DEFAULT now(),
  -- notice that there are cases that people don't want other people know the referred one is the same person
  -- possible for users who want to disclose what he/she can provide without disclosing it is themselves
  can_do_referral BOOLEAN NOT NULL DEFAULT FALSE,
  CHECK (NOT (target_user_id IS NULL AND can_do_referral IS TRUE)),
  is_private_note BOOLEAN NOT NULL GENERATED ALWAYS AS (target_user_id IS NOT NULL) stored
);


#!/bin/bash

echo "creating folders if not exists..."
ssh sql.friensco.com "mkdir db_migration;mkdir db_dump"
last_file_num=`ssh sql.friensco.com "ls db_migration | tail -1"`
if [ $last_file_num = "" ]
then
    last_file_num=0
fi
echo $last_file_num
current_file_num=`ls db/00_to_99/ | tail -1 | cut -d'_' -f 1`
if [ $last_file_num = $current_file_num ]
then
    echo "production server up-to-date. nothing to change"
else
    echo "backing up db and then doing migration"
    scp -r db/00_to_99/ sql.friensco.com:db_migration/
    ssh sql.friensco.com "mv db_migration/00_to_99 db_migration/$current_file_num ; pg_dump -c -C friensco_prod > db_dump/${current_file_num}.sql"
    ssh sql.friensco.com "ls db_migration/$current_file_num | tail -n $(($current_file_num - $last_file_num)) | xargs -I % psql -f db_migration/$current_file_num/% -d friensco_prod"
fi


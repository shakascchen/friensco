import { HelpData, VoluntaryData, VoluntaryState } from '../utils/types';

export const voluntaryList: VoluntaryData[] = [
    {
        startingOfVoluntaryIsAccepted: true,
        voluntaryState: VoluntaryState.DONE,
        feedbackScoreFromHelpee: 7,
        createdAt: "三天前",
        volunteer: {
            displayName: "Green Bin",
            avatarUrl: "https://gravatar.com/avatar/bd3428bfe46a6166a9e6f1248d2c7086?s=400&d=robohash&r=x",
        },

    },
    {
        startingOfVoluntaryIsAccepted: false,
        voluntaryState: VoluntaryState.ABORT,
        createdAt: "五天前",
        volunteer: {
            displayName: "紫衣機器人",
            avatarUrl: "https://gravatar.com/avatar/6a413bf2688b9fea6de28172ff463bc1?s=400&d=robohash&r=x",
        },

    },
    {
        voluntaryState: VoluntaryState.INIT,
        createdAt: "七天前",
        volunteer: {
            displayName: "洗衣架",
            avatarUrl: "https://gravatar.com/avatar/80a8612b3f66d88eb57c405b8125a739?s=400&d=robohash&r=x",
        },

    },

];

export const helpList: HelpData[] = [
    {
        contact: {
            displayName: "ABC",
            avatarUrl: "https://akveo.github.io/react-native-ui-kitten/docs/assets/playground-build/static/media/icon.a78e4b51.png",
        },
        helpRole: "HELPER",
        utilityName: "見面聊天",
        utilityTags: ["人際關係", "實體活動", "聊天"],
        utilityProperties: {
            形式: "散步",
            長度: "2 小時",
        },
        description: "可以找我聊天",
    },
    {
        contact: {
            displayName: "普羅米",
            avatarUrl: "https://gravatar.com/avatar/185dfdb179a0a55497c6d6820de4fb0e?s=400&d=robohash&r=x",
        },
        helpRole: "HELPEE",
        utilityName: "徵女友",
        utilityTags: ["人際關係", "交友", "伴侶"],
        utilityProperties: {
            目標: "結婚",
            期望類型: "可愛賢妻",
            年齡: "32 歲以下",
            底線: "不要背債",
        },
        description: "拜託朋友幫忙介紹",
    },
    {
        contact: {
            displayName: "讚讚的",
            avatarUrl: "https://gravatar.com/avatar/a639290b33368d0f9274a1c1048b6e54?s=400&d=robohash&r=x",
        },
        helpRole: "HELPER",
        utilityName: "SpaceX 投資機會",
        utilityTags: ["投資", "創投", "基金"],
        utilityProperties: {
            方式: "組 fund",
            聯絡: "私下聯絡",
            最小單位: "私下聯絡",
        },
        description: "有投資 SpaceX 的機會喔！要一起組 fund 的來聯絡我",
    },
    {
        contact: {
            displayName: "Forest Country",
            avatarUrl: "https://gravatar.com/avatar/794c07c84efc2abe7850233299e26200?s=400&d=robohash&r=x",
        },
        helpRole: "HELPEE",
        utilityName: "moedict.tw 字的寫法有錯誤",
        utilityTags: ["公民參與", "open source", "網路辭典", "中文資源", "問題"],
        utilityProperties: {
            錯誤: "汛寫成污",
            幫忙方式: "哪裡回報錯誤或是哪裡可以直接幫忙改？",
        },
        description: null,
    },

];

// lack of property 'balance'
export const contact = {
    email: "somebody@somewhere.nowhere",
    cellphone: "+123456789",
    displayName: "喬達摩",
    avatarUrl: "https://akveo.github.io/react-native-ui-kitten/docs/assets/playground-build/static/media/icon.a78e4b51.png",
    namesByLanguage: {
        en: {
            firstName: "Joe",
            middleName: "Da",
            lastName: "Moore",
        },
        zh: {
            姓: "喬",
            名: "達摩",
        },
    },
    dateOfAcquaintance: "2017/5",
    storyOfAcquaintance: "南投深山靜坐活動",
    caresAbout: ["身心靈", "正義", "知識"],
    likes: ["鳳梨酥", "印度", "旅行"],
    dislikes: ["時代力量", "蚊子"],
    activities: ["靜坐", "冥想", "瑜珈", "齋戒"],
    moreColumns: {
        信仰: "無神論",
        MBTI: "INFJ",
    },
    contactAddresses: [
        { type: 'real_estate', usage: 'office', address: '藍藍路一段三號 2 樓' },
    ],
    occupations: [
        { title: "業務專員", organization_name: "喬寶" },
    ],
};

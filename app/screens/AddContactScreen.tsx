import React from 'react';
import { ImageProps, SafeAreaView, ScrollView } from 'react-native';
import { Text, Spinner, Button, Icon, TopNavigationAction, TopNavigation, Layout } from '@ui-kitten/components';
import * as Contacts from 'expo-contacts';
import { PermissionStatus } from 'expo-contacts';

const BackIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> => (
  <Icon name='arrow-back' {...props} />
);

const AddContactScreen: React.FC<{
  navigation?: any
}> = ({ navigation }) => {
  const navigateBack = () => {
    navigation.goBack();
  };

  const BackAction = () => (
    <TopNavigationAction icon={BackIcon} onPress={navigateBack} />
  );

  const [userContacts, setUserContacts] = React.useState<Contacts.Contact[] | null>(null);

  if (!userContacts) {
    Contacts.requestPermissionsAsync()
      .then(({ status }) => {
        if (status === PermissionStatus.GRANTED) {
          Contacts.getContactsAsync()
            .then(({ data }) => {
              setUserContacts(data);
            });
        }
      });
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <TopNavigation title='Friensco' alignment='center' accessoryLeft={BackAction} />
      {userContacts
        ?
        <Layout style={{ flex: 1 }} level="2">
          <ScrollView>
            {userContacts.map((contactUser, index) =>
              <Button
                key={index}
                style={{ justifyContent: "space-between" }}
                accessoryLeft={_ => <Text>{contactUser.name}</Text>}
                appearance="ghost"
                status="basic"
                size="small"

              />
            )
            }
          </ScrollView>
        </Layout>
        :
        <Layout style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
          <Spinner size="giant" />
        </Layout>
      }
    </SafeAreaView >
  );
}

export default AddContactScreen;

import React from 'react';
import { ImageProps, View, ScrollView, StyleSheet } from 'react-native';
import { Spinner, Button, Layout, TopNavigation, Icon, Text, Popover } from '@ui-kitten/components';
import { mutateWithAuthToken, queryWithAuthToken } from '../utils/apollo-client';
import { connect } from 'react-redux';
import { RootState } from '../reducers/reducers';
import { Contacts_virtualUsers_nodes, Contacts } from '../gql-types/Contacts';
import { contactsByUserId, importContact } from '../utils/gql-document';
import * as ExpoContacts from 'expo-contacts';
import { PermissionStatus } from 'expo-contacts';
import { ImportContact } from '../gql-types/ImportContact';
import Constants from 'expo-constants';

const CONTACT_BATCH_SIZE = Number(Constants.manifest.extra.contactBatchSize);

const SearchIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> => (
  <Icon name='search' {...props} />
);

const ReloadIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> => (
  <Icon name='sync-outline' {...props} />
);

const ContactDetailIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> => (
  <Icon name='arrow-ios-forward-outline' {...props} />
);

const HomeScreen: React.FC<{
  currentUserId?: number | null
  , navigation?: any
}> = ({ currentUserId, navigation }) => {

  const [isUserContactsLoaded, setIsUserContactsLoaded] = React.useState<boolean>(false);
  const [userContacts, setUserContacts] = React.useState<Contacts_virtualUsers_nodes[]>([]);
  const [popoverMessage, setPopoverMessage] = React.useState('');

  if (!isUserContactsLoaded) {
    queryWithAuthToken<Contacts>(
      contactsByUserId,
      { userId: currentUserId || 0 },
      queryResult => {
        setUserContacts(queryResult.data.virtualUsers?.nodes || []);
        setIsUserContactsLoaded(true);
      }
    );
  }

  return (
    <View style={{ flex: 1 }}>
      <TopNavigation
        title='   Friensco'
        alignment='start'
        accessoryRight={_ =>
          <Button
            appearance="ghost"
            accessoryLeft={ReloadIcon}
            size="small"
            status="control"
            onPress={_ => {
              ExpoContacts.requestPermissionsAsync()
                .then(({ status }) => {
                  if (status === PermissionStatus.GRANTED) {
                    ExpoContacts.getContactsAsync()
                      .then(({ data }) => {
                        const nameNotNullData = data.filter(function nameNull(value) { return typeof (value.name as any) !== "undefined" });
                        var batchData: ExpoContacts.Contact[][] = [];
                        const batchLength = Math.ceil(nameNotNullData.length / CONTACT_BATCH_SIZE);
                        for (let i = 0; i < batchLength; i++) {
                          if (i === batchLength - 1) {
                            batchData.push(nameNotNullData.slice(i * CONTACT_BATCH_SIZE));
                          } else {
                            batchData.push(nameNotNullData.slice(i * CONTACT_BATCH_SIZE, (i + 1) * CONTACT_BATCH_SIZE - 1));
                          }
                        }
                        batchData.map((contactData) =>
                          mutateWithAuthToken<ImportContact>(
                            importContact,
                            { contactData },
                            _ => setIsUserContactsLoaded(false),
                            rejectedReason => setPopoverMessage(JSON.stringify(rejectedReason))
                          ));
                      });

                  }
                });
            }
            }
          >Sync from phone</Button>
        }
      />
      < Popover
        backdropStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.5)' }}
        visible={popoverMessage !== ''}
        onBackdropPress={() => setPopoverMessage('')}
        anchor={() => <View />}>
        <Text category="h6" style={{ margin: 20 }}>
          {popoverMessage}
        </Text>
      </Popover >
      {
        isUserContactsLoaded
          ?
          <Layout style={styles.rootLayout
          } level="2" >
            <ScrollView>
              {userContacts.map((contactUser, index) =>
                <Button
                  key={index}
                  style={{ justifyContent: "space-between" }}
                  accessoryLeft={_ => <Text>{contactUser.displayName}</Text>}
                  accessoryRight={ContactDetailIcon}
                  appearance="ghost"
                  status="basic"
                  size="small"
                  onPress={_ => {
                    navigation.navigate('Contact', { contactUser, shouldSendQuery: true });
                    navigation.navigate('Contact', { contactUser, shouldSendQuery: false });
                  }}
                />
              )
              }
            </ScrollView>
          </Layout >
          :
          <Layout style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
            <Spinner size="giant" />
          </Layout>
      }

    </View >
  );
};

export default connect(
  (state: RootState) => ({
    currentUserId: state.currentUser.id
  }),
  null
)(HomeScreen);

const styles = StyleSheet.create({
  rootLayout: {
    flex: 1,
  },
  contact: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 4,
  },
});

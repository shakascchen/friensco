import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from './HomeScreen';
import AddContactScreen from './AddContactScreen';
import ContactScreen from './ContactScreen';

const { Navigator, Screen } = createStackNavigator();

const HomeNavigator = () => (
  <Navigator headerMode='none'>
    <Screen name='Home' component={HomeScreen} />
    <Screen name='AddContact' component={AddContactScreen} />
    <Screen name='Contact' component={ContactScreen} />
  </Navigator>
);

export default HomeNavigator;

import React from 'react';
import { StyleSheet, View, ScrollView } from 'react-native';
import { Layout, Popover, Text, Input, Button } from '@ui-kitten/components';
import { gql } from '@apollo/client';
import { apolloClient } from '../utils/apollo-client';
import { setItemAsync } from 'expo-secure-store';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { CurrentUserState } from '../utils/types';
import { getCurrentUserId } from '../actions/current-user';

const mapDispatchToProps = (dispatch: Dispatch) => {
  return bindActionCreators({ getCurrentUserId }, dispatch);
}

const LoginScreen: React.FC<{
  setIsInSignUpScreen: React.Dispatch<React.SetStateAction<boolean>>,
  getCurrentUserId: () => (dispatch: Dispatch, _currentUserIdState: CurrentUserState) => Promise<void>;
}
> = (props) => {

  const [userEmail, setUserEmail] = React.useState('');
  const [userPassword, setUserPassword] = React.useState('');
  const [inputErrorMessage, setInputErrorMessage] = React.useState<{ emailErrorMessage?: string, passwordErrorMessage?: string }>({});
  const [dialogMessage, setDialogMessage] = React.useState<string>('');

  return (
    <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flexGrow: 1 }} >
      <Layout style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <View style={styles.title}>
          <Text style={{ marginVertical: 2 }} category='h1' status="control">Friensco</Text>
          <Text style={{ marginVertical: 2 }} category='s1' >Remember and Recall Friends' Anything</Text>
        </View>
        <Popover
          backdropStyle={styles.backdrop}
          visible={dialogMessage !== ''}
          anchor={() => <View />}
          onBackdropPress={() => setDialogMessage('')}>
          <Text category="h6" style={{ margin: 20 }}>
            {dialogMessage}
          </Text>
        </Popover>
        <View style={styles.loginInputRow}>
          <View style={{ flex: 1 }} />
          <Input
            style={{ flex: 6 }}
            label="Account Email"
            caption={inputErrorMessage.emailErrorMessage}
            status={inputErrorMessage.emailErrorMessage && 'danger'}
            value={userEmail}
            placeholder='Email Address'
            onChangeText={nextValue => setUserEmail(nextValue.replace(/ /g, '').toLowerCase())}
          />
          <View style={{ flex: 1 }} />
        </View>
        <View style={styles.loginInputRow}>
          <View style={{ flex: 1 }} />
          <Input
            style={{ flex: 6 }}
            label="Password"
            caption={inputErrorMessage.passwordErrorMessage}
            status={inputErrorMessage.passwordErrorMessage && 'danger'}
            value={userPassword}
            placeholder='Password'
            secureTextEntry={true}
            onChangeText={nextValue => setUserPassword(nextValue)}
          />
          <View style={{ flex: 1 }} />
        </View>
        <View style={styles.buttonRow}>
          <View style={{ flex: 2 }} />
          <Button
            status="basic"
            style={styles.button}
            onPress={_ => props.setIsInSignUpScreen(true)}
          > Sign-Up</Button>
          <View style={{ flex: 1 }} />
          <Button
            style={styles.button}
            onPress={_e => {
              if (userEmail === '' || userPassword === '') {
                setInputErrorMessage({
                  emailErrorMessage: userEmail === '' ? "Can't be blank" : undefined,
                  passwordErrorMessage: userPassword === '' ? "Can't be blank" : undefined,
                });
              } else {

                apolloClient.mutate({
                  mutation: gql`mutation LoginMutation($email: String!, $password: String!) {
                      authenticate(input: {email: $email, password: $password}) {
                        jwtToken
                      }
                    }`,
                  variables: {
                    email: userEmail,
                    password: userPassword,
                  }
                }).then(result => {
                  if (result.data && result.data.authenticate && result.data.authenticate.jwtToken) {
                    console.log('process login');
                    setItemAsync('authToken', result.data.authenticate.jwtToken as string).then(_ => {

                      (props.getCurrentUserId as unknown as () => Promise<void>)(); // thunk action will be dispatched by redux-thunk
                    });

                  } else {
                    setInputErrorMessage({
                      emailErrorMessage: 'Email or password incorrect. Or account unverified',
                      passwordErrorMessage: 'Email or password incorrect. Or account unverified',
                    });
                  }
                }, rejectedReason => {
                  console.log(rejectedReason);
                  setDialogMessage(String(rejectedReason));
                });

              };
            }}
          >Sign-In</Button>
          <View style={{ flex: 2 }} />
        </View>
      </Layout >
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  loginInputRow: {
    flexDirection: 'row',
    marginVertical: 3,
  },
  buttonRow: {
    flexDirection: 'row',
    marginVertical: 20,
  },
  button: {
    flex: 4,
  },
  title: {
    marginVertical: 28,
    alignItems: 'center',
  },
});

export default connect(
  null,
  mapDispatchToProps
)(LoginScreen);

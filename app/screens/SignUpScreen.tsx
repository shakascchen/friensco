import React from 'react';
import { StyleSheet, View, ScrollView } from 'react-native';
import { Popover, Layout, Text, Input, Button, CheckBox } from '@ui-kitten/components';
import { apolloClient } from '../utils/apollo-client';
import { SignUp, SignUpVariables } from '../gql-types/SignUp';
import { signUpMutation } from '../utils/gql-document';
import { commonStyles } from '../utils/styles';
import { openBrowserAsync } from 'expo-web-browser';
import { setItemAsync } from 'expo-secure-store';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { CurrentUserState } from '../utils/types';
import { getCurrentUserId } from '../actions/current-user';

const InputRow = (props: any) => (
  <View style={styles.loginInputRow}>
    <View style={styles.rowMarginSpace} />
    <View style={props.inputStyle}>
      {props.children}
    </View>
    <View style={styles.rowMarginSpace} />
  </View>
);

const mapDispatchToProps = (dispatch: Dispatch) => {
  return bindActionCreators({ getCurrentUserId }, dispatch);
}

const email_rex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

const SignUpScreen: React.FC<{
  setIsInSignUpScreen: React.Dispatch<React.SetStateAction<boolean>>,
  getCurrentUserId: () => (dispatch: Dispatch, _currentUserIdState: CurrentUserState) => Promise<void>;
}> = (props) => {

  const [userEmail, setUserEmail] = React.useState('');
  const [userPassword, setUserPassword] = React.useState('');
  const [inputErrorMessage, setInputErrorMessage] = React.useState<{ emailErrorMessage?: string, passwordErrorMessage?: string, usernameErrorMessage?: string, termsAndConditions?: 'danger' }>({});
  const [username, setUserName] = React.useState('');
  const [dialogMessage, setDialogMessage] = React.useState<string>('');
  const [checked, setChecked] = React.useState(false);

  return (
    <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flexGrow: 1 }} >
      <Layout style={{ flex: 1, alignItems: 'center' }}>
        <View style={styles.title}>
          <Text style={{ marginVertical: 2 }} category='h1' status="control">Friensco</Text>
          <Text style={{ marginVertical: 2 }} category='s1' >Friendship and helping each other</Text>
        </View>
        <InputRow inputStyle={styles.rowContent}>
          <Input

            label="Account Email"
            caption={inputErrorMessage.emailErrorMessage}
            status={inputErrorMessage.emailErrorMessage && 'danger'}
            value={userEmail}
            placeholder='Email Address'
            onChangeText={nextValue => setUserEmail(nextValue.replace(/ /g, '').toLowerCase())}
          />
        </InputRow>
        <InputRow inputStyle={styles.rowContent}>
          <Input

            label="Account Name"
            caption={inputErrorMessage.usernameErrorMessage}
            status={inputErrorMessage.usernameErrorMessage && 'danger'}
            value={username}
            placeholder='name'
            onChangeText={nextValue => setUserName(nextValue)}
          />
        </InputRow>
        <Popover
          backdropStyle={styles.backdrop}
          visible={dialogMessage != ''}
          anchor={() => <View />}
          onBackdropPress={() => setDialogMessage('')}>
          <Text category="h6" style={{ margin: 20 }}>
            {dialogMessage}
          </Text>
        </Popover>
        <InputRow inputStyle={styles.rowContent}>
          <Input

            label="Password"
            caption={inputErrorMessage.passwordErrorMessage}
            status={inputErrorMessage.passwordErrorMessage && 'danger'}
            value={userPassword}
            placeholder='Password'
            secureTextEntry={true}
            onChangeText={nextValue => setUserPassword(nextValue)}
          />
        </InputRow>
        <InputRow inputStyle={[commonStyles.flexRowJustifyContentCenterAlignItemsCenter, { flex: 14 }]}>
          <CheckBox
            style={{ flex: 1 }}
            checked={checked}
            status={inputErrorMessage.termsAndConditions}
            onChange={nextChecked => setChecked(nextChecked)} />
          <Text status={inputErrorMessage.termsAndConditions} style={{ flex: 8 }}>  I've read and accepted</Text>
          <Button
            status="control"
            appearance="ghost"
            size="small"
            style={{ flex: 7 }}
            onPress={_ => openBrowserAsync('https://friensco.com/friensco-app-terms-conditions/')}>Terms and Conditions</Button>
        </InputRow>
        <View style={styles.signUpButtonRow}>
          <View style={styles.rowMarginSpace} />
          <Button
            size="giant"
            style={styles.rowContent}
            onPress={_e => {
              if (userEmail === '' || userPassword === '' || username == '' || !checked) {
                checked || setDialogMessage('You have to read and accept terms and conditions to register');
                setInputErrorMessage({
                  emailErrorMessage: userEmail === '' ? "Can't be blank" : undefined,
                  passwordErrorMessage: userPassword === '' ? "Can't be blank" : undefined,
                  usernameErrorMessage: username === '' ? "Can't be blank" : undefined,
                  termsAndConditions: !checked ? "danger" : undefined
                });
              } else {
                setInputErrorMessage({});
                if (!(email_rex.test(userEmail))) {
                  setInputErrorMessage({
                    emailErrorMessage: 'Not a valid email address'
                  });
                } else {

                  apolloClient.mutate<SignUp, SignUpVariables>({
                    mutation: signUpMutation,
                    variables: { userEmail, userPassword, username },
                  }).then(result => {
                    const jwtToken = result.data?.signup?.jwtToken;

                    if (jwtToken) {
                      setItemAsync('authToken', jwtToken as string).then(_ => {

                        (props.getCurrentUserId as unknown as () => Promise<void>)(); // thunk action will be dispatched by redux-thunk
                      });

                    } else {
                      setDialogMessage('Registration done. \nPlease wait for verification');
                    }
                  }, rejectedReason => {
                    if (rejectedReason.graphQLErrors instanceof Array) {
                      const errors = rejectedReason.graphQLErrors;
                      if (errors.length > 0 && errors[0]?.message?.startsWith("duplicate key value")) {
                        setInputErrorMessage({
                          emailErrorMessage: 'Email already in use',
                        });

                      } else {
                        setDialogMessage(String(rejectedReason));
                      }

                    } else {
                      setDialogMessage(String(rejectedReason));
                    }
                  });
                }
              }
            }}
          > Register as an early user</Button>
          <View style={styles.rowMarginSpace} />
        </View>
        <InputRow inputStyle={styles.signInButton}>
          <Button
            size="small"
            status="basic"
            onPress={_ => props.setIsInSignUpScreen(false)}
          >Back to Sign-In</Button>
        </InputRow>
      </Layout >
    </ScrollView >
  );
};

export default connect(
  null,
  mapDispatchToProps
)(SignUpScreen);

const styles = StyleSheet.create({
  loginInputRow: {
    flexDirection: 'row',
    marginVertical: 3,
  },
  signUpButtonRow: {
    flexDirection: 'row',
    marginVertical: 20,
  },
  signInButton: {
    flex: 1,
  },
  rowContent: {
    flex: 6,
  },
  rowMarginSpace: {
    flex: 1
  },
  title: {
    marginTop: 64,
    marginBottom: 20,
    alignItems: 'center',
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
});

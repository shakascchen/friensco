import React, { useState, useCallback } from 'react';
import { SafeAreaView, ImageProps, View } from 'react-native';
import { TopNavigationAction, TopNavigation, Icon, Popover, Text } from '@ui-kitten/components';
import { GiftedChat, IMessage } from 'react-native-gifted-chat';
import { connect } from 'react-redux';
import { Dispatch, bindActionCreators } from 'redux';
import { RootState } from '../reducers/reducers';
import { mutateWithAuthToken, subscribeWithAuthToken } from '../utils/apollo-client';
import { CurrentUserState } from '../utils/types';
import { DirectMessageSubscription, DirectMessageSubscriptionVariables } from '../gql-types/DirectMessageSubscription';
import { DirectMessageMutation } from '../gql-types/DirectMessageMutation';
import { directMessageMutation, directMessageSubscription } from '../utils/gql-document';
import { logout } from '../actions/current-user';

const BackIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> => (
  <Icon name='arrow-back' {...props} />
);

const ChannelScreen: React.FC<{
  logout: () => (dispatch: Dispatch, _currentUserIdState: CurrentUserState) => Promise<void>,
  route?: any,
  currentUserId?: number | null
  , navigation?: any
}> = ({ currentUserId, navigation, route, logout }) => {
  const [messages, setMessages] = useState<IMessage[]>([]);
  const [subscriber, setSubscriber] = React.useState<ZenObservable.Subscription | null>(null);
  const [errorMessage, setErrorMessage] = React.useState<string>('');
  const targetUserId = route?.params?.targetUserId || 1;

  const subscriptionErrorHandler = (error: any) => {
    console.log(error);
    if (error?.message === "jwt expired") {
      (logout as unknown as () => Promise<void>)(); // thunk action will be dispatched by redux-thunk
    } else {
      setErrorMessage(error);
    }
  };
  if (!subscriber) {
    subscribeWithAuthToken<DirectMessageSubscription, DirectMessageSubscriptionVariables>(
      directMessageSubscription,
      { userId: currentUserId || 0, targetUserId },
      value => {
        setMessages(value.data?.directMessages?.nodes?.map(nodes => ({ ...nodes, _id: nodes.id, user: { _id: nodes.user?.id || 0, name: nodes.user?.name || '', avatar: nodes.user?.avatarUrl || '' } })) || []);
      },
      setSubscriber,
      subscriptionErrorHandler,
    );
  }

  const navigateBack = () => {
    // there is a chance that subscriber is not ready yet.
    subscriber?.unsubscribe();
    navigation.goBack();
  };

  const BackAction = () => (
    <TopNavigationAction icon={BackIcon} onPress={navigateBack} />
  );

  /* useEffect(() => {
   *   setMessages([
   *     {
   *       _id: 3,
   *       text: 'Hbello developer',
   *       createdAt: new Date(),
   *       user: {
   *         _id: 1,
   *         name: 'React Native',
   *         avatar: 'https://placeimg.com/140/140/any',
   *       },
   *     },
   *     {
   *       _id: 2,
   *       text: 'another user',
   *       createdAt: new Date(),
   *       user: {
   *         _id: 4,
   *         name: 'React Native',
   *         avatar: 'https://placeimg.com/140/140/any',
   *       },
   *     },
   *   ])
   * }, []);
     onSend={messages => onSend(messages)}
   */

  const onSend = useCallback((messages: IMessage[]) => {
    mutateWithAuthToken<DirectMessageMutation>(
      directMessageMutation,
      {
        userId: currentUserId || 0,
        targetUserId,
        text: messages.map(message => message.text).reduce((previousValue, currentValue) => previousValue + '\n' + currentValue),
      },
      _ => _,
      rejectedReason => {
        if (rejectedReason.networkError?.statusCode !== 401) {
          setErrorMessage(JSON.stringify(rejectedReason).substr(0, 100));
        }
      }
    );

  }, []);

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <TopNavigation title='Friensco' alignment='center' accessoryLeft={BackAction} />
      <Popover
        backdropStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.5)' }}
        visible={errorMessage != ''}
        anchor={() => <View />}
        onBackdropPress={() => setErrorMessage('')}>
        <Text category="h6" style={{ margin: 20 }}>
          {errorMessage}
        </Text>
      </Popover>
      <GiftedChat
        messages={messages}
        onSend={onSend}
        user={{
          _id: currentUserId || 0,
        }}
      />
    </SafeAreaView>
  )
};

export default connect(
  (state: RootState) => ({
    currentUserId: state.currentUser.id
  }),
  (dispatch: Dispatch) => bindActionCreators({ logout }, dispatch),
)(ChannelScreen);

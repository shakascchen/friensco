import React from 'react';
import { Popover, TopNavigation, TopNavigationAction, Icon, Layout, Text, Input, Button } from '@ui-kitten/components';
import { SafeAreaView, ImageProps, View } from 'react-native';
import { StyleSheet } from 'react-native';
import { queryWithAuthToken, mutateWithAuthToken } from '../utils/apollo-client';
import { connect } from 'react-redux';
import { RootState } from '../reducers/reducers';
import { createFriendship, userIdByEmail } from '../utils/gql-document';
import { UserIdByEmail } from '../gql-types/UserIdByEmail';
import { CreateFriendship } from '../gql-types/CreateFriendship';

const BackIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> => (
  <Icon name='arrow-back' {...props} />
);

const email_rex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

const AddContactScreen: React.FC<{
  currentUserId?: number | null
  , navigation?: any
}> = ({ navigation, currentUserId }) => {
  const [targetUserEmail, setTargetUserEmail] = React.useState('');
  const [errorMessage, setErrorMessage] = React.useState<string | undefined>(undefined);
  const [popoverMessage, setPopoverMessage] = React.useState('');

  const navigateBack = () => {
    navigation.goBack();
  };

  const BackAction = () => (
    <TopNavigationAction icon={BackIcon} onPress={navigateBack} />
  );
  const SubmitButton = () => {
    return (
      <Button
        style={styles.rows}
        onPress={_ => {
          if (targetUserEmail === "") {
            setErrorMessage("Can't be blank");
          } else if (!(email_rex.test(targetUserEmail))) {
            setErrorMessage('Not a valid email address');
          } else {
            setErrorMessage('');
            queryWithAuthToken<UserIdByEmail>(
              userIdByEmail,
              {
                email: targetUserEmail
              },
              result => {
                if (result.data.userByEmail?.id) {
                  const targetId = result.data.userByEmail.id;
                  mutateWithAuthToken<CreateFriendship>(
                    createFriendship,
                    {
                      sourceId: currentUserId as number,
                      targetId
                    },
                    result => {
                      if (result.data?.createFriendship?.friendship?.createdAt) {
                        setPopoverMessage('Friend request sent');
                      } else {
                        setPopoverMessage('Unknown error');
                      }
                    },
                    rejectedReason => {

                      if (rejectedReason.graphQLErrors && rejectedReason.graphQLErrors[0]) {

                        if (rejectedReason.graphQLErrors[0].message.startsWith("duplicate key value violates unique")) {
                          setPopoverMessage("Friend request already sent or already added to contacts");
                        } else {
                          setPopoverMessage(rejectedReason);
                        }

                      } else {
                        setPopoverMessage(JSON.stringify(rejectedReason));
                      }
                    }
                  );
                } else {
                  setPopoverMessage(`No user with email: ${targetUserEmail}`);
                }

              });
          }
        }}
      > Submit
      </Button >);
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <TopNavigation title='Friensco' alignment='center' accessoryLeft={BackAction} />
      <Layout style={{ flex: 1, alignItems: 'center' }}>
        <Text style={styles.rows} category='h2'>Add a Contact</Text>
        <View style={[styles.rows, { flexDirection: 'row' }]}>
          <View style={{ flex: 1 }} />
          <Input
            style={{ flex: 6 }}
            value={targetUserEmail}
            status={errorMessage && 'danger'}
            caption={errorMessage}
            placeholder='Email'
            onChangeText={nextValue => setTargetUserEmail(nextValue)}
          />
          <View style={{ flex: 1 }} />
        </View>
        <Popover
          backdropStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.5)' }}
          visible={popoverMessage !== ''}
          onBackdropPress={() => setPopoverMessage('')}
          anchor={SubmitButton}>
          <Text category="h6" style={{ margin: 20 }}>
            {popoverMessage}
          </Text>
        </Popover>
      </Layout>
    </SafeAreaView >
  );
};
const styles = StyleSheet.create({
  rows: {
    marginVertical: 12
  }
});

export default connect(
  (state: RootState) => ({
    currentUserId: state.currentUser.id
  }),
  null
)(AddContactScreen);

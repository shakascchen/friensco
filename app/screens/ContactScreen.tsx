import React from 'react';
import { Spinner, TopNavigation, TopNavigationAction, Icon, Layout, Text, Input, Button } from '@ui-kitten/components';
import { SafeAreaView, ImageProps, View, ScrollView } from 'react-native';
import { StyleSheet } from 'react-native';
import { Contacts_virtualUsers_nodes } from '../gql-types/Contacts';
import { connect } from 'react-redux';
import { RootState } from '../reducers/reducers';
import { commonStyles } from '../utils/styles';
import { ContactById, ContactById_virtualUser_propertiesByPropertyGroupVirtualUserIdAndPropertyId_nodes } from '../gql-types/ContactById';
import { queryWithAuthToken } from '../utils/apollo-client';
import { contactById } from '../utils/gql-document';
import Property from '../components/Property';
import PropertyDetail from '../components/PropertyDetail';

const BackIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> => (
  <Icon name='arrow-back' {...props} />
);

const AddIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> => (
  <Icon name='file-add-outline' {...props} />
);

const ContactScreen: React.FC<{
  currentUserId?: number | null,
  navigation?: any,
  route: { params: { contactUser: Contacts_virtualUsers_nodes, shouldSendQuery: boolean } }
}> = ({ navigation, currentUserId, route: { params: { contactUser, shouldSendQuery } } }) => {

  const navigateBack = () => {
    navigation.goBack();
  };

  const BackAction = () => (
    <TopNavigationAction icon={BackIcon} onPress={navigateBack} />
  );

  const [contactUserProperties, setContactUserProperties] = React.useState<ContactById_virtualUser_propertiesByPropertyGroupVirtualUserIdAndPropertyId_nodes[] | null>(null);
  const [propertyDetailParams, setPropertyDetailParams] = React.useState<{ index?: number } | null>(null);

  if (!contactUserProperties || shouldSendQuery) {
    queryWithAuthToken<ContactById>(
      contactById,
      { id: contactUser.id },
      result => setContactUserProperties(result.data.virtualUser?.propertiesByPropertyGroupVirtualUserIdAndPropertyId.nodes || [])
    )
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <TopNavigation title='Friensco' alignment='center' accessoryLeft={BackAction} />
      { (propertyDetailParams !== null) && contactUserProperties ?
        <PropertyDetail
          editing
          style={{ flex: 1 }}
          propertyData={typeof propertyDetailParams.index === 'number' ? contactUserProperties[propertyDetailParams.index] : undefined}
          contactUser={contactUser}
          backAction={() => {
            setPropertyDetailParams(null);
            setContactUserProperties(null);
          }}
        />
        :
        <Layout style={{ flex: 1 }} level="2">
          <ScrollView>
            <View style={[commonStyles.flexRowJustifyContentCenterAlignItemsCenter, { height: commonStyles.deviceInfo.height * 0.08 }]}>
              <View style={{ flex: 1 }} />
              <Text style={{ flex: 3 }}>{contactUser.displayName}</Text>
              <Button
                style={{ flex: 1 }}
                appearance="ghost"
                status="control"
                size="large"
                accessoryRight={AddIcon}
                onPress={_ =>
                  setPropertyDetailParams({})
                }
              />
              <View style={{ flex: 0.25 }} />
            </View>
            {
              contactUserProperties ?
                contactUserProperties.map((property, index) => <Property key={index} propertyData={property} height={commonStyles.deviceInfo.height * 0.15} onPress={() => setPropertyDetailParams({ index })} />
                )
                :
                <Spinner size="giant" />
            }

          </ScrollView>
        </Layout>
      }
    </SafeAreaView>
  );

};

export default connect(
  (state: RootState) => ({
    currentUserId: state.currentUser.id
  }),
  null
)(ContactScreen);

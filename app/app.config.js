import 'dotenv/config';

if (typeof process.env.PRODUCTION === 'undefined') {
  throw new Error('Environment variable $PRODUCTION has to be set.');
};

if (process.env.PRODUCTION !== 'true' && process.env.CONSOLE_LOG_GRAPHQL_URL === 'true') {
  console.log(process.env.DEV_GRAPHQL_URL);
}

export default ({ config }) => {

  return {
    ...config,
    extra: {
      graphqlUrl: process.env.PRODUCTION === 'true' ? process.env.GRAPHQL_URL : process.env.DEV_GRAPHQL_URL,
      wsUri: process.env.PRODUCTION === 'true' ? process.env.GRAPHQL_WS_URI : process.env.DEV_GRAPHQL_WS_URI,
      contactBatchSize: process.env.PRODUCTION === 'true' ? process.env.CONTACT_BATCH_SIZE : process.env.DEV_CONTACT_BATCH_SIZE,
    }
  };
};

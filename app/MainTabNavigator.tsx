import React from 'react';
import { ImageProps, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Spinner, BottomNavigation, BottomNavigationTab, Layout, Text, Icon, Button } from '@ui-kitten/components';
import HomeNavigator from './screens/HomeNavigator';
import { commonStyles } from './utils/styles';
import { currentUser } from './utils/gql-document';
import { CurrentUser, CurrentUser_getCurrentUser } from './gql-types/CurrentUser';
import { deleteItemAsync } from 'expo-secure-store';
import { queryWithAuthToken } from './utils/apollo-client';
import { store } from './utils/redux';
import { LOGOUT } from './utils/types';

const { Navigator, Screen } = createBottomTabNavigator();

const HomeIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> => (
  <Icon name='people-outline' {...props} />
);


const SettingsIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> => (
  <Icon name='settings-outline' {...props} />
);

const SettingsScreen = () => {
  const [meUser, setMeUser] = React.useState<CurrentUser_getCurrentUser | null>(null);

  if (!meUser) {
    queryWithAuthToken<CurrentUser>(
      currentUser,
      undefined,
      result => setMeUser(result.data.getCurrentUser)
    );
  }

  return meUser ?
    <Layout style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <View style={{ flex: 2 }} />
      <View style={[commonStyles.flexRowJustifyContentCenterAlignItemsCenter, { flex: 1 }]}>
        <View style={{ flex: 1 }} />
        <Text style={{ flex: 3 }} category="h4">Account Email: </Text>
        <Text style={{ flex: 3 }}>{meUser.email}</Text>
        <View style={{ flex: 1 }} />
      </View>
      <View style={[commonStyles.flexRowJustifyContentCenterAlignItemsCenter, { flex: 1 }]}>
        <View style={{ flex: 1 }} />
        <Text style={{ flex: 3 }} category="h4">Account Name: </Text>
        <Text style={{ flex: 3 }}>{meUser.name}</Text>
        <View style={{ flex: 1 }} />
      </View>
      <Button onPress={_ => {
        deleteItemAsync('authToken').then(_ =>
          store.dispatch({
            type: LOGOUT,
            payload: { id: null }
          })
        )
      }
      }
      >Logout</Button>
      <View style={{ flex: 2 }} />
    </Layout>
    :
    <Spinner size="giant" />;

}

const BottomTabBar = ({ navigation, state }: any) => (
  <BottomNavigation
    selectedIndex={state.index}
    onSelect={index => navigation.navigate(state.routeNames[index])}>
    <BottomNavigationTab icon={HomeIcon} />
    <BottomNavigationTab icon={SettingsIcon} />
  </BottomNavigation>
);

const BottomNavigator = () => (
  <Navigator tabBar={props => <BottomTabBar {...props} />}>
    <Screen name='Home' component={HomeNavigator} />
    <Screen name='Settings' component={SettingsScreen} />
  </Navigator>
);

/* const BottomNavigatorAfterConnect = connect(
 *   (state: RootState) => ({
 *     currentUserSelfContactId: state.currentUser.selfContactId
 *   }),
 *   null
 * )(BottomNavigator);*/


const MainTabNavigator = () => (
  <NavigationContainer>
    <BottomNavigator />
  </NavigationContainer>
);

export default MainTabNavigator;

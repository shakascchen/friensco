import { combineReducers } from 'redux';
import { CurrentUserAction, CurrentUserState, GET_CURRENT_USER_ID, LOGOUT } from '../utils/types';

const currentUser: (state: CurrentUserState, action: CurrentUserAction) => CurrentUserState = (state = { id: null }, action) => {
    console.log('am I dispatched?');
    switch (action.type) {
        case GET_CURRENT_USER_ID:
            return Object.assign({}, action.payload);
        case LOGOUT:
            return Object.assign({}, action.payload);
        default:
            return state;
    }
}

const rootReducer = combineReducers({ currentUser });

export type RootState = {
    currentUser: CurrentUserState
};

export default rootReducer;

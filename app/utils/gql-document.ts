import { gql } from '@apollo/client';


export const currentUser = gql`query CurrentUser {
  getCurrentUser {
    avatarUrl
    id
    name
    featuredPhotoUrls
    email
  }
}`;

export const signUpMutation = gql`mutation SignUp($userEmail: String!, $username: String!, $userPassword: String!) {
  signup(
    input: {email: $userEmail, name: $username, password: $userPassword}
  ) {
    jwtToken
  }
}`;

export const createPropertyGroup = gql`mutation CreatePropertyGroup($propertyId: Int!, $virtualUserId: Int!) {
  createPropertyGroup(
    input: {propertyGroup: {propertyId: $propertyId, virtualUserId: $virtualUserId}}
  ) {
    propertyGroup {
      propertyId
      virtualUserId
    }
  }
}`;

export const createProperty = gql`mutation CreateProperty($title: String!, $tags: [String!]!, $description: String!, $userId: Int!) {
  createProperty(
    input: {property: {userId: $userId, title: $title, tags: $tags, description: $description}}
  ) {
    property {
      id
    }
  }
}`;

export const updateProperty = gql`mutation UpdateProperty($id: Int!, $patch: PropertyPatch = {}) {
  updateProperty(input: {patch: $patch, id: $id}) {
    property {
      description
      id
      tags
      title
      urls
    }
  }
}`;

export const deletePropertyGroup = gql`mutation DeletePropertyGroup($propertyId: Int!, $virtualUserId: Int!) {
  deletePropertyGroup(
    input: {propertyId: $propertyId, virtualUserId: $virtualUserId}
  ) {
    propertyGroup {
      propertyId
      virtualUserId
    }
  }
}`;

export const deletePropertyById = gql`mutation DeletePropertyById($id: Int!) {
  deleteProperty(input: {id: $id}) {
    property {
      id
    }
  }
}`;

export const contactsByUserId = gql`query Contacts($userId: Int!) {
  virtualUsers(condition: {userId: $userId}) {
    nodes {
      id
      displayName
    }
  }
}`;

export const contactById = gql`query ContactById($id: Int!) {
  virtualUser(id: $id) {   
      id
      displayName
      propertiesByPropertyGroupVirtualUserIdAndPropertyId {
        nodes {
          id
          title
          tags
          urls
          description
      }
    }
  }  
}`;

export const importContact = gql`mutation ImportContact($contactData: JSON!) {
  syncVirtualUser(input: {contactData: $contactData}) {
    clientMutationId
  }
}`;

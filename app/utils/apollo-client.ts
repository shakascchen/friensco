import Constants from 'expo-constants';
import { ApolloClient, InMemoryCache, ApolloQueryResult, FetchResult, DefaultOptions, HttpLink } from '@apollo/client';
import { ServerError } from '@apollo/client/link/utils';
import { onError } from '@apollo/client/link/error';
import { deleteItemAsync, getItemAsync } from 'expo-secure-store';
import { DocumentNode } from 'graphql';
import { WebSocketLink } from "@apollo/client/link/ws";
import { store } from './redux';
import { LOGOUT } from './types';

console.log('Manifest extra:');
console.log(Constants.manifest.extra);
// deleteItemAsync('authToken');

const defaultOptions: DefaultOptions = {
    watchQuery: {
        fetchPolicy: 'no-cache',
        errorPolicy: 'ignore',
    },
    query: {
        fetchPolicy: 'no-cache',
        errorPolicy: 'all',
    },
};

const httpLink = new HttpLink({
    uri: Constants.manifest.extra.graphqlUrl
});

const logoutLink = onError(({ networkError }) => {
    if ((networkError as ServerError)?.statusCode === 401) {
        // force logout
        deleteItemAsync('authToken').then(_ =>
            store.dispatch({
                type: LOGOUT,
                payload: { id: null }
            })
        )
    }
});

export const apolloClient = new ApolloClient({
    link: logoutLink.concat(httpLink),
    cache: new InMemoryCache(),
    defaultOptions,
});

export function queryWithAuthToken<T>(
    query: DocumentNode,
    variables: Record<string, any> | undefined,
    onQueryResult: (value: ApolloQueryResult<T>) => void | PromiseLike<void>
) {
    getItemAsync('authToken').then(authToken =>
        apolloClient.query<T>({
            context: {
                headers: {
                    authorization: `Bearer ${authToken}`
                }
            },
            query,
            variables,
        }).then(onQueryResult));
};

export function mutateWithAuthToken<T>(
    mutation: DocumentNode,
    variables: Record<string, any> | undefined,
    onMutationResult: (value: FetchResult<T, Record<string, any>, Record<string, any>>) => FetchResult<T, Record<string, any>, Record<string, any>> | null | undefined | void,
    onReject?: (reason: any) => void | PromiseLike<void>
) {
    getItemAsync('authToken').then(authToken =>
        apolloClient.mutate<T>({
            context: {
                headers: {
                    authorization: `Bearer ${authToken}`
                }
            },
            mutation,
            variables,
        }).then(onMutationResult, onReject));
};

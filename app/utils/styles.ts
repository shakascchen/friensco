import { Dimensions, StyleSheet } from 'react-native';

export const commonStyles = StyleSheet.create({
    flexRowJustifyContentCenterAlignItemsCenter: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
    },
    deviceInfo: {
        width: Dimensions.get("window").width,
        height: Dimensions.get("window").height,
    },
});

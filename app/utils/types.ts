
export type CurrentUserState = { id: number | null };

export type CurrentUserAction = { type: string, payload: CurrentUserState };

export const GET_CURRENT_USER_ID = 'GET_CURRENT_USER_ID';

export const LOGOUT = 'LOGOUT';

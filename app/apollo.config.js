
module.exports = {
  client: {
    includes: ['./**/*.ts', './**/*.tsx'],
    service: {
      name: 'friensco-server',
      localSchemaFile: '../server/schema.graphql'
    }
  }
};

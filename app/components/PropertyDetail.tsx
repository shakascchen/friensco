import React from 'react';
import { StyleSheet, View, ImageProps, ScrollView, ViewStyle, StyleProp } from 'react-native';
import { Popover, Icon, Divider, Button, Text, Layout, Input } from '@ui-kitten/components';
import { connect } from 'react-redux';
import { RootState } from '../reducers/reducers';
import { commonStyles } from '../utils/styles';
import { mutateWithAuthToken } from '../utils/apollo-client';
import { deletePropertyById, updateProperty, createProperty, deletePropertyGroup, createPropertyGroup } from '../utils/gql-document';
import { DeletePropertyGroup } from '../gql-types/DeletePropertyGroup';
import { UpdateProperty } from '../gql-types/UpdateProperty';
import { CreateProperty } from '../gql-types/CreateProperty';
import { ContactById_virtualUser_propertiesByPropertyGroupVirtualUserIdAndPropertyId_nodes } from '../gql-types/ContactById';
import { Contacts_virtualUsers_nodes } from '../gql-types/Contacts';
import { DeletePropertyById } from '../gql-types/DeletePropertyById';
import { CreatePropertyGroup } from '../gql-types/CreatePropertyGroup';

const EditIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> => (
  <Icon name='edit-outline' {...props} />
);

const DeleteIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> => (
  <Icon name='trash-2-outline' {...props} />
);

const BackIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> => (
  <Icon name='arrow-back' {...props} />
);

const UploadIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> => (
  <Icon name='upload-outline' {...props} />
);

//TODO: use state to control expand inner content or not.
//TODO: use props to configure as HelpPost or HelpNote
const PropertyDetail: React.FC<{ style: StyleProp<ViewStyle>, contactUser: Contacts_virtualUsers_nodes, propertyData?: ContactById_virtualUser_propertiesByPropertyGroupVirtualUserIdAndPropertyId_nodes, backAction?: () => void, editing?: boolean, currentUserId?: number }> = (props) => {

  const [title, setTitle] = React.useState<string>(props.propertyData?.title || 'untitled');

  const [tags, setTags] = React.useState<string[]>(
    props.propertyData?.tags.filter<string>(function notNull(value): value is string { return value !== null }) || []
  );
  const [description, setDescription] = React.useState<string>(props.propertyData?.description || '');
  const [titleInput, setTitleInput] = React.useState<string>(title);
  const [tagInput, setTagInput] = React.useState(tags.reduce((previousValue, currentValue, currentIndex) => (currentIndex === 0 ? currentValue : (previousValue + ',' + currentValue)), ''));
  const [descriptionInput, setDescriptionInput] = React.useState(description);
  const [dialogMessage, setDialogMessage] = React.useState<string>('');

  const submitAction = () => {
    if (props.propertyData?.id) {
      const propertyId = props.propertyData.id;
      mutateWithAuthToken<UpdateProperty>(
        updateProperty,
        {
          id: propertyId,
          patch: {
            title,
            description,
            tags,
          },
        },
        _ => {
          props.backAction && props.backAction();
        },
        rejectedReason => {
          setDialogMessage(JSON.stringify(rejectedReason));
        }
      );
    } else if (props.currentUserId) {
      const userId = props.currentUserId;
      mutateWithAuthToken<CreateProperty>(
        createProperty,
        {
          userId,
          title,
          description,
          tags,
        },
        result => {
          const propertyId = result.data?.createProperty?.property?.id;
          if (propertyId) {
            mutateWithAuthToken<CreatePropertyGroup>(
              createPropertyGroup,
              {
                propertyId,
                virtualUserId: props.contactUser.id,
              },
              _ => {
                props.backAction && props.backAction();
                setTitle('untitled');
                setDescription('');
                setTags([]);
                setDescriptionInput('');
                setTagInput('');
              }
            );
          } else {
            setDialogMessage('Failed to create note.');
          }
        },
        rejectedReason => {
          setDialogMessage(JSON.stringify(rejectedReason));
        }
      );
    } else {
      setDialogMessage('Failed to submit new information.');
    }

  }

  return (
    <Layout style={props.style} level="2">
      <Popover
        backdropStyle={styles.backdrop}
        visible={dialogMessage !== ''}
        anchor={() => <View />}
        onBackdropPress={() => setDialogMessage('')}>
        <Text category="h6" style={{ margin: 20 }}>
          {dialogMessage}
        </Text>
      </Popover>
      <ScrollView
        style={{ flex: 1 }}
        showsHorizontalScrollIndicator={false}
      >
        {props.backAction &&
          <View style={{ alignSelf: 'stretch', flexDirection: 'row' }}>
            <Button
              style={{ flex: 1 }}
              appearance="ghost"
              status="control"
              accessoryLeft={BackIcon}
              onPress={props.backAction}
            />
            {
              props.editing && props.propertyData &&
              <Button
                style={{ flex: 1 }}
                appearance="ghost"
                status="danger"
                accessoryLeft={DeleteIcon}
                onPress={_ => {
                  if (props.propertyData && props.backAction) {
                    const propertyId = props.propertyData.id;
                    const backAction = props.backAction;
                    mutateWithAuthToken<DeletePropertyGroup>(
                      deletePropertyGroup,
                      { propertyId, virtualUserId: props.contactUser.id },
                      _ => {
                        mutateWithAuthToken<DeletePropertyById>(
                          deletePropertyById,
                          { id: propertyId },
                          _ => {
                          },
                          rejectedReason => console.log(rejectedReason)
                        );
                        backAction();
                      },
                      rejectedReason => console.log(rejectedReason)
                    );
                  }
                }
                }
              />
            }
          </View>
        }
        <Layout level="3" style={{ padding: 6, marginHorizontal: 8 }}>
          <View style={{ margin: 2 }}>
            {props.editing ?
              <Input
                label="Title"
                value={titleInput}
                placeholder='title...'
                onChangeText={nextValue => setTitleInput(nextValue)}
                onBlur={_ => setTitle(titleInput)}
              />
              :
              <Text style={{ alignSelf: 'center' }}>{title}</Text>
            }
          </View>
          <View style={styles.propertyHeader} >
            {props.editing ?
              <Input
                label="Tags"
                value={tagInput}
                placeholder='separated by commas'
                onChangeText={nextValue => setTagInput(nextValue.replace(/[,\s]*,[,\s]*/g, ',').replace(/^[,\s]+/g, ''))}
                onBlur={_ => {
                  setTags(tagInput === "" ? [] : tagInput.replace(/[\s]+$/g, '').split(',').filter(function notEmpty(value) { return value !== ""; }));
                  setTagInput(tagInput.replace(/[\s]+$/g, ''));
                }}
              />
              :
              <View style={styles.tags} >
                {
                  tags.map((tag, index) => <Button key={index} style={{ margin: 3 }} appearance="outline" size="small">{tag}</Button>)
                }
              </View>
            }
          </View>
          <Divider style={{ marginVertical: 6 }} />
          {props.editing ?
            <Input
              label="Description"
              value={descriptionInput}
              onChangeText={nextValue => setDescriptionInput(nextValue)}
              onBlur={_ => setDescription(descriptionInput)}
              multiline
            />
            :
            <Text>{description}</Text>
          }
        </Layout >
        {props.editing &&
          <View>
            <Divider style={{ marginVertical: 6 }} />
            <View style={commonStyles.flexRowJustifyContentCenterAlignItemsCenter}>
              <View style={{ flex: 1 }} />
              <Button
                style={{ flex: 2 }}
                onPress={submitAction}
              >Submit</Button>
              <View style={{ flex: 1 }} />
            </View>
          </View>
        }
      </ScrollView>
    </Layout >
  );
};

export default connect(
  (state: RootState) => ({
    currentUserId: state.currentUser.id
  }),
  null
)(PropertyDetail);


const styles = StyleSheet.create({
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  propertyName: {
    flex: 3,
  },
  tags: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  propertyHeader: {
    flexDirection: 'row',
    margin: 2,
    justifyContent: 'flex-start'
  },
});

import React from 'react';
import { StyleSheet, View, ImageProps, ScrollView, ImageBackground, StyleProp, ViewStyle } from 'react-native';
import { Input, Icon, Avatar, Button, Text, Layout } from '@ui-kitten/components';
import { connect } from 'react-redux';
import { RootState } from '../reducers/reducers';
import { AllFriendsQuery_friendships_nodes_target, AllFriendsQuery_friendships_nodes_target_firstProperties_nodes } from '../gql-types/AllFriendsQuery';
import Property from './Property';
import PropertyDetail from './PropertyDetail';
import { Asset } from 'expo-asset';
import { AllFriendsVirtualUsersQuery_virtualUsersWhoFriendsCanDoReferralToCurrentUser_nodes } from '../gql-types/AllFriendsVirtualUsersQuery';
import { HelpAttitude } from '../gql-types/globalTypes';
import { commonStyles } from '../utils/styles';
import { updateUser } from '../utils/gql-document';
import { mutateWithAuthToken } from '../utils/apollo-client';
import { UpdateUser } from '../gql-types/UpdateUser';

const MessageIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> => (
  <Icon name='message-square-outline' {...props} />
);

const UploadIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> => (
  <Icon name='upload-outline' {...props} />
);

const PropertyDetailIcon = (props?: Partial<ImageProps>): React.ReactElement<ImageProps> => (
  <Icon name='arrow-ios-downward' {...props} />
);

const anonymImageUri = Asset.fromModule(require('../assets/images/anonym.png')).uri;

//TODO: use state to control expand inner content or not.
//TODO: use props to configure as HelpPost or HelpNote
const Contact: React.FC<{ style?: StyleProp<ViewStyle>, navigation?: any, contactUser: AllFriendsQuery_friendships_nodes_target | AllFriendsVirtualUsersQuery_virtualUsersWhoFriendsCanDoReferralToCurrentUser_nodes, width: number, currentUserId?: number, editable?: boolean, reloadContact?: () => void }> = (props) => {

  const [selectedPropertyForDetail, setSelectedPropertyForDetail] = React.useState<AllFriendsQuery_friendships_nodes_target_firstProperties_nodes | null>(null);

  const [imageUrlInput, setImageUrlInput] = React.useState<{ index: number | null, url: string }>({ index: null, url: '' });

  const [featuredPhotoUrls, setFeaturedPhotoUrls] = React.useState<string[]>((props.contactUser.featuredPhotoUrls?.filter<string>(function notNull(value): value is string { return value !== null }) || []));

  const unSetPropertyForDetail = () => {
    if (props.reloadContact) {
      props.reloadContact();
    }
    setSelectedPropertyForDetail(null);
  };

  const firstProperties = props.contactUser.firstProperties.nodes;
  const secondProperties = props.contactUser.secondProperties.nodes;

  return (
    <Layout style={[props.style, styles.root, { width: props.width }]} level="2" >
      <View style={styles.featuredPhotos}>
        <ScrollView
          horizontal
          contentContainerStyle={{ width: props.width * (featuredPhotoUrls.length + (props.editable ? 1 : 0)) }}>
          {featuredPhotoUrls.map((photoUrl, index) =>
            <ImageBackground key={index} resizeMode="cover" style={{ width: props.width }} source={{ uri: photoUrl }} >
              {props.editable && (index === imageUrlInput.index ?
                <>
                  <View style={{ flex: 1 }} />
                  <Input
                    autoFocus
                    label="replace with"
                    value={imageUrlInput.url}
                    placeholder='image url'
                    onChangeText={nextValue => setImageUrlInput({ index, url: nextValue })}
                    onBlur={_ => {
                      const newPhotoUrls = featuredPhotoUrls.map((url, i) => (i === index ? imageUrlInput.url : url));
                      if (props.currentUserId) {
                        mutateWithAuthToken<UpdateUser>(
                          updateUser,
                          {
                            id: props.currentUserId as unknown as number,
                            patch: { featuredPhotoUrls: newPhotoUrls }
                          },
                          _ => { },
                          rejectedReason => console.log(rejectedReason)
                        )
                      };

                      setFeaturedPhotoUrls(newPhotoUrls);
                      setImageUrlInput({ index: null, url: '' });
                    }
                    }
                  />
                  <View style={{ flex: 1 }} />
                </>
                :
                <View style={{ flex: 1, alignItems: "flex-end" }}>
                  <Button
                    style={{ flex: 1 }}
                    onPress={_ => setImageUrlInput({ index, url: photoUrl })}
                  >Replace</Button>
                  <Button
                    style={{ flex: 1 }}
                    status="danger"
                    onPress={_ => {
                      const newPhotoUrls = featuredPhotoUrls.length === 1 ? [] : featuredPhotoUrls.slice(0, index).concat(featuredPhotoUrls.slice(index + 1));
                      if (props.currentUserId) {
                        mutateWithAuthToken<UpdateUser>(
                          updateUser,
                          {
                            id: props.currentUserId as unknown as number,
                            patch: { featuredPhotoUrls: newPhotoUrls }
                          },
                          _ => { },
                          rejectedReason => console.log(rejectedReason)
                        )
                      };
                      setFeaturedPhotoUrls(newPhotoUrls);
                    }}
                  >Delete</Button>
                  <View style={{ flex: 6 }} />
                </View>)
              }
            </ImageBackground>
          )}
          {props.editable && (
            imageUrlInput.index === featuredPhotoUrls.length + 1 ?
              <View style={{ flex: 1 }}>
                <View style={{ flex: 1 }} />
                <Input
                  autoFocus
                  label="new image"
                  value={imageUrlInput.url}
                  placeholder='image url'
                  onChangeText={nextValue => setImageUrlInput({ index: imageUrlInput.index, url: nextValue })}
                  onBlur={_ => {
                    if (props.currentUserId) {
                      mutateWithAuthToken<UpdateUser>(
                        updateUser,
                        {
                          id: props.currentUserId as unknown as number,
                          patch: { featuredPhotoUrls: featuredPhotoUrls.concat([imageUrlInput.url]) }
                        },
                        _ => { },
                        rejectedReason => console.log(rejectedReason)
                      )
                    };

                    setFeaturedPhotoUrls(featuredPhotoUrls.concat([imageUrlInput.url]));
                    setImageUrlInput({ index: null, url: '' });
                  }
                  }
                />
                <View style={{ flex: 1 }} />
              </View>
              :
              <View style={[commonStyles.flexRowJustifyContentCenterAlignItemsCenter, { flex: 1 }]}>
                <Button
                  size="giant"
                  accessoryLeft={UploadIcon}
                  onPress={_ => setImageUrlInput({ index: featuredPhotoUrls.length + 1, url: '' })}
                />
              </View>
          )
          }
        </ScrollView>
      </View>
      {
        (() => {
          switch (props.contactUser.__typename) {
            case "User":
              return (
                <View style={styles.contactNavigation}>
                  <Avatar {...{ resizeMode: "contain" }} style={{ flex: 2 }} shape="round" source={{ uri: props.contactUser.avatarUrl }} />
                  <Text style={{ flex: 7, alignSelf: "center" }}>{props.contactUser.name}</Text>
                  {props.currentUserId && props.contactUser.id !== props.currentUserId && <Button style={{ flex: 1 }} appearance="ghost" accessoryLeft={MessageIcon} onPress={_ => props.navigation.navigate('InstantMessenger', { targetUserId: props.contactUser.id })} />
                  }
                </View>
              );
            case "VirtualUser":
              const targetUserId = props.contactUser.user?.id || 0;
              return (
                <View style={[styles.contactNavigation, { flex: 2 }]}>
                  <View style={{ flex: 2 }} >
                    <Avatar {...{ resizeMode: "contain" }} style={{ flex: 1, alignSelf: "center" }} shape="round" source={{ uri: anonymImageUri }} />

                    <View style={{ flex: 1, flexDirection: "row", alignItems: "center", marginLeft: 8 }}>
                      <Text>a friend of </Text>
                    </View>
                  </View>
                  <View style={{ flex: 7, justifyContent: "center" }}>
                    <View style={{ flex: 1, flexDirection: "row", alignItems: "center" }}>
                      <Text>{props.contactUser.displayName + ' (anonym)'}</Text>
                    </View>
                    <View style={{ flex: 1, flexDirection: "row", alignItems: "center" }}>
                      <Avatar {...{ resizeMode: "contain" }} style={{ flex: 1 }} shape="round" source={{ uri: props.contactUser.user?.avatarUrl || '' }} />
                      <Text style={{ flex: 5 }}>{props.contactUser.user?.name || 'loading...'}</Text>
                      {props.currentUserId && props.contactUser.id !== props.currentUserId &&
                        <Button style={{ flex: 1 }} appearance="ghost" accessoryLeft={MessageIcon} onPress={_ => props.navigation.navigate('InstantMessenger', { targetUserId })} />
                      }
                    </View>
                  </View >
                </View >
              );
          }
        })()

      }
      {
        !selectedPropertyForDetail &&
        <Layout style={styles.propertiesBody} level="2" >
          <ScrollView
            style={{ flex: 1, marginHorizontal: 10 }}
            nestedScrollEnabled={true}
            contentContainerStyle={{
              height: (styles.property.height + styles.propertyInterval.height) * (firstProperties.length + secondProperties.length) + styles.header.height * (Math.ceil(firstProperties.length / (firstProperties.length + 1)) + Math.ceil(secondProperties.length / (firstProperties.length + 1))) // any non zero length leads to 1 for length/(length+1)
            }}
            showsHorizontalScrollIndicator={false}
          >
            {
              firstProperties.length > 0 &&
              <View style={styles.header}>
                <Text status="warning" category="h4">{HelpAttitude.CALL_FOR_HELP.replace(/_/g, ' ').toLowerCase()}</Text>
              </View>
            }
            {firstProperties.map((propertyNode, index) => {

              return (
                <View key={index}>
                  <View style={{ flexDirection: 'row' }}>
                    <Property propertyData={propertyNode} width={props.width - styles.propertyDetailButtonParamOnly.width} height={styles.property.height} />
                    <Layout level="3" style={{ justifyContent: 'center' }}>
                      <Button
                        appearance="ghost"
                        size="small"
                        status="control"
                        accessoryLeft={PropertyDetailIcon}
                        onPress={_ => setSelectedPropertyForDetail(propertyNode)}
                      />
                    </Layout>
                  </View>
                  <View style={styles.propertyInterval} />
                </View>);
            })

            }
            {
              secondProperties.length > 0 &&
              <View style={styles.header}>
                <Text status="control" category="h6">{HelpAttitude.ABOUT_ME.replace(/_/g, ' ').toLowerCase()}</Text>
              </View>
            }
            {secondProperties.map((propertyNode, index) => {

              return (
                <View key={index}>
                  <View style={{ flexDirection: 'row' }}>
                    <Property propertyData={propertyNode} width={props.width - styles.propertyDetailButtonParamOnly.width} height={styles.property.height} />
                    <Layout level="3" style={{ justifyContent: 'center' }}>
                      <Button
                        appearance="ghost"
                        size="small"
                        status="control"
                        accessoryLeft={PropertyDetailIcon}
                        onPress={_ => setSelectedPropertyForDetail(propertyNode)}
                      />
                    </Layout>
                  </View>
                  <View style={styles.propertyInterval} />
                </View>);
            })

            }

          </ScrollView>
        </Layout >
      }
      {
        selectedPropertyForDetail &&
        <PropertyDetail style={styles.propertiesBody} width={props.width} propertyData={selectedPropertyForDetail} backAction={unSetPropertyForDetail} editable={props.editable} />
      }
    </Layout >
  );

};

export default connect(
  (state: RootState) => ({
    currentUserId: state.currentUser.id
  }),
  null
)(Contact);


const styles = StyleSheet.create({
  featuredPhotos: {
    flex: 4
  },
  header: {
    height: commonStyles.deviceInfo.height * 0.04,
  },
  contactNavigation: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 8,
  },
  nameAndAvatar: {
    flex: 9,
    flexDirection: 'row',
  },
  helpAttitude: {
    flex: 7,
  },
  root: {
    marginVertical: 6,
  },
  propertiesBody: {
    flex: 5,
  },
  property: {
    height: commonStyles.deviceInfo.height * 0.25,
  },
  propertyInterval: {
    height: 5,
  },
  propertyDetailButtonParamOnly: {
    width: 80,
  },
});

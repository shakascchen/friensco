import React from 'react';
import { View, StyleSheet, Pressable } from 'react-native';
import { Text, Layout, Button } from '@ui-kitten/components';
import { ContactById_virtualUser_propertiesByPropertyGroupVirtualUserIdAndPropertyId_nodes } from '../gql-types/ContactById';

const Property: React.FC<{ propertyData: ContactById_virtualUser_propertiesByPropertyGroupVirtualUserIdAndPropertyId_nodes, height: number, onPress?: () => void }> = ({ propertyData, height, onPress }) => {

  const tags: string[] = propertyData.tags.filter<string>(function notNull(value): value is string { return value !== null });

  return (
    <Pressable onPress={onPress}>
      <View style={{ height }}>
        <Layout style={{ flex: 5 }} level="3">
          <View style={styles.column}>
            <Text style={{ alignSelf: 'center' }}>{propertyData.title || ''}</Text>
          </View>
          <View style={[styles.column, { flexDirection: "row", alignItems: "center" }]} >
            {
              tags.map((tag, index) => <Button key={index} style={{ margin: 3 }} appearance="outline" size="small">{tag}</Button>)
            }
          </View>
          <View style={styles.column}>
            <Text>{propertyData.description || ''}</Text>
          </View>
        </Layout >
        <View style={{ flex: 1 }} />
      </View>
    </Pressable>
  );
};

export default Property;

const styles = StyleSheet.create({
  column: {
    flex: 1,
    marginVertical: 4,
  },
});

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the UI Kitten TypeScript template
 * https://github.com/akveo/react-native-ui-kitten
 *
 * Documentation: https://akveo.github.io/react-native-ui-kitten/docs
 *
 * @format
 */

import React from 'react';
import * as eva from '@eva-design/eva';
import { ApplicationProvider, IconRegistry } from '@ui-kitten/components';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import { default as theme } from './theme.json';
import { apolloClient as _ } from './utils/apollo-client'; // cached apolloClient, see https://nodejs.org/api/modules.html#modules_caching
import { store } from './utils/redux';
import { Provider, connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { RootState } from './reducers/reducers';
import MainTabNavigator from './MainTabNavigator';
import LoginScreen from './screens/LoginScreen';
import SignUpScreen from './screens/SignUpScreen';
import { SafeAreaView } from 'react-native';
import { getCurrentUserId } from './actions/current-user';
import { CurrentUserState } from './utils/types';

const mapStateToProps = (state: RootState) => {
  console.log('what state?');
  console.log(state);
  return {
    currentUserId: state.currentUser.id
  };
}

const AppEntry: React.FC<{
  currentUserId: number | null,
  getCurrentUserId: () => (dispatch: Dispatch, _currentUserIdState: CurrentUserState) => Promise<void>
}> = ({ currentUserId, getCurrentUserId }) => {
  console.log(currentUserId);
  const [isInSignUpScreen, setIsInSignUpScreen] = React.useState<boolean>(false);

  getCurrentUserId();
  return (
    <SafeAreaView style={{ flex: 1 }}>
      {!currentUserId ?
        (isInSignUpScreen ?
          <SignUpScreen setIsInSignUpScreen={setIsInSignUpScreen} />
          :
          <LoginScreen setIsInSignUpScreen={setIsInSignUpScreen} />
        )
        :
        <MainTabNavigator />
      }
    </SafeAreaView>
  );
}

const AppEntryConnected = connect(
  mapStateToProps,
  (dispatch: Dispatch) => {
    return bindActionCreators({ getCurrentUserId }, dispatch);
  }
)(AppEntry)

export default function App() {

  return (
    <Provider store={store}>
      <IconRegistry icons={EvaIconsPack} />
      <ApplicationProvider {...eva} theme={{ ...eva.dark, ...theme }}>
        <AppEntryConnected />
      </ApplicationProvider>
    </Provider>);
};

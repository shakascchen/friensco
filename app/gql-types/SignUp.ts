/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: SignUp
// ====================================================

export interface SignUp_signup {
  __typename: "SignupPayload";
  jwtToken: any | null;
}

export interface SignUp {
  signup: SignUp_signup | null;
}

export interface SignUpVariables {
  userEmail: string;
  username: string;
  userPassword: string;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PropertyPatch } from "./globalTypes";

// ====================================================
// GraphQL mutation operation: UpdateProperty
// ====================================================

export interface UpdateProperty_updateProperty_property {
  __typename: "Property";
  description: string;
  id: number;
  tags: (string | null)[];
  title: string;
  urls: (string | null)[];
}

export interface UpdateProperty_updateProperty {
  __typename: "UpdatePropertyPayload";
  /**
   * The `Property` that was updated by this mutation.
   */
  property: UpdateProperty_updateProperty_property | null;
}

export interface UpdateProperty {
  /**
   * Updates a single `Property` using a unique key and a patch.
   */
  updateProperty: UpdateProperty_updateProperty | null;
}

export interface UpdatePropertyVariables {
  id: number;
  patch?: PropertyPatch | null;
}

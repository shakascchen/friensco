/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ContactById
// ====================================================

export interface ContactById_virtualUser_propertiesByPropertyGroupVirtualUserIdAndPropertyId_nodes {
  __typename: "Property";
  id: number;
  title: string;
  tags: (string | null)[];
  urls: (string | null)[];
  description: string;
}

export interface ContactById_virtualUser_propertiesByPropertyGroupVirtualUserIdAndPropertyId {
  __typename: "VirtualUserPropertiesByPropertyGroupVirtualUserIdAndPropertyIdManyToManyConnection";
  /**
   * A list of `Property` objects.
   */
  nodes: ContactById_virtualUser_propertiesByPropertyGroupVirtualUserIdAndPropertyId_nodes[];
}

export interface ContactById_virtualUser {
  __typename: "VirtualUser";
  id: number;
  displayName: string;
  /**
   * Reads and enables pagination through a set of `Property`.
   */
  propertiesByPropertyGroupVirtualUserIdAndPropertyId: ContactById_virtualUser_propertiesByPropertyGroupVirtualUserIdAndPropertyId;
}

export interface ContactById {
  virtualUser: ContactById_virtualUser | null;
}

export interface ContactByIdVariables {
  id: number;
}

/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CreateProperty
// ====================================================

export interface CreateProperty_createProperty_property {
  __typename: "Property";
  id: number;
}

export interface CreateProperty_createProperty {
  __typename: "CreatePropertyPayload";
  /**
   * The `Property` that was created by this mutation.
   */
  property: CreateProperty_createProperty_property | null;
}

export interface CreateProperty {
  /**
   * Creates a single `Property`.
   */
  createProperty: CreateProperty_createProperty | null;
}

export interface CreatePropertyVariables {
  title: string;
  tags: string[];
  description: string;
  userId: number;
}

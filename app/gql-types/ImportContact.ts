/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: ImportContact
// ====================================================

export interface ImportContact_syncVirtualUser {
  __typename: "SyncVirtualUserPayload";
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId: string | null;
}

export interface ImportContact {
  syncVirtualUser: ImportContact_syncVirtualUser | null;
}

export interface ImportContactVariables {
  contactData: any;
}

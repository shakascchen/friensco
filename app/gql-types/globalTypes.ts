/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

/**
 * Represents an update to a `Property`. Fields that are set will be updated.
 */
export interface PropertyPatch {
  userId?: number | null;
  isClosed?: boolean | null;
  createdAt?: any | null;
  tags?: (string | null)[] | null;
  urls?: (string | null)[] | null;
  description?: string | null;
  isPrivate?: boolean | null;
  title?: string | null;
}

//==============================================================
// END Enums and Input Objects
//==============================================================

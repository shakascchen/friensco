/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: LoginMutation
// ====================================================

export interface LoginMutation_authenticate {
  __typename: "AuthenticatePayload";
  jwtToken: any | null;
}

export interface LoginMutation {
  authenticate: LoginMutation_authenticate | null;
}

export interface LoginMutationVariables {
  email: string;
  password: string;
}

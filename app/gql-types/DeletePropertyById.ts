/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeletePropertyById
// ====================================================

export interface DeletePropertyById_deleteProperty_property {
  __typename: "Property";
  id: number;
}

export interface DeletePropertyById_deleteProperty {
  __typename: "DeletePropertyPayload";
  /**
   * The `Property` that was deleted by this mutation.
   */
  property: DeletePropertyById_deleteProperty_property | null;
}

export interface DeletePropertyById {
  /**
   * Deletes a single `Property` using a unique key.
   */
  deleteProperty: DeletePropertyById_deleteProperty | null;
}

export interface DeletePropertyByIdVariables {
  id: number;
}

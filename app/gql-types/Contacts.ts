/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: Contacts
// ====================================================

export interface Contacts_virtualUsers_nodes {
  __typename: "VirtualUser";
  id: number;
  displayName: string;
}

export interface Contacts_virtualUsers {
  __typename: "VirtualUsersConnection";
  /**
   * A list of `VirtualUser` objects.
   */
  nodes: Contacts_virtualUsers_nodes[];
}

export interface Contacts {
  /**
   * Reads and enables pagination through a set of `VirtualUser`.
   */
  virtualUsers: Contacts_virtualUsers | null;
}

export interface ContactsVariables {
  userId: number;
}

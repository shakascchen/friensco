/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeletePropertyGroup
// ====================================================

export interface DeletePropertyGroup_deletePropertyGroup_propertyGroup {
  __typename: "PropertyGroup";
  propertyId: number;
  virtualUserId: number;
}

export interface DeletePropertyGroup_deletePropertyGroup {
  __typename: "DeletePropertyGroupPayload";
  /**
   * The `PropertyGroup` that was deleted by this mutation.
   */
  propertyGroup: DeletePropertyGroup_deletePropertyGroup_propertyGroup | null;
}

export interface DeletePropertyGroup {
  /**
   * Deletes a single `PropertyGroup` using a unique key.
   */
  deletePropertyGroup: DeletePropertyGroup_deletePropertyGroup | null;
}

export interface DeletePropertyGroupVariables {
  propertyId: number;
  virtualUserId: number;
}

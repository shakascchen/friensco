/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: CurrentUser
// ====================================================

export interface CurrentUser_getCurrentUser {
  __typename: "User";
  avatarUrl: string;
  id: number;
  name: string;
  featuredPhotoUrls: (string | null)[] | null;
  email: string;
}

export interface CurrentUser {
  getCurrentUser: CurrentUser_getCurrentUser | null;
}

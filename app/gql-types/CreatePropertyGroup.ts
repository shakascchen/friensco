/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CreatePropertyGroup
// ====================================================

export interface CreatePropertyGroup_createPropertyGroup_propertyGroup {
  __typename: "PropertyGroup";
  propertyId: number;
  virtualUserId: number;
}

export interface CreatePropertyGroup_createPropertyGroup {
  __typename: "CreatePropertyGroupPayload";
  /**
   * The `PropertyGroup` that was created by this mutation.
   */
  propertyGroup: CreatePropertyGroup_createPropertyGroup_propertyGroup | null;
}

export interface CreatePropertyGroup {
  /**
   * Creates a single `PropertyGroup`.
   */
  createPropertyGroup: CreatePropertyGroup_createPropertyGroup | null;
}

export interface CreatePropertyGroupVariables {
  propertyId: number;
  virtualUserId: number;
}

import { apolloClient } from '../utils/apollo-client';
import { getItemAsync, deleteItemAsync } from 'expo-secure-store';
import { Dispatch } from 'redux';
import { gql } from '@apollo/client';
import { CurrentUserState, GET_CURRENT_USER_ID, LOGOUT } from '../utils/types';

export const getCurrentUserId = () => {
    return async (dispatch: Dispatch, _currentUserIdState: CurrentUserState) => {
        try {
            console.log('am I here?');
            const authToken = await getItemAsync('authToken');
            if (authToken) {
                const result = await apolloClient.query<{ getCurrentUserId: number | null }>({
                    context: {
                        headers: {
                            authorization: `Bearer ${authToken}`
                        }
                    },
                    query: gql`query CurrentUserIdQuery { getCurrentUserId }`
                });

                dispatch({ type: GET_CURRENT_USER_ID, payload: { id: result.data?.getCurrentUserId } });
            } else {
                dispatch({ type: GET_CURRENT_USER_ID, payload: { id: null } });
            }
        } catch (e) {
            console.log(e)
        }
    }
};

export const logout = () => {
    return async (dispatch: Dispatch, _currentUserIdState: CurrentUserState) => {
        try {
            await deleteItemAsync('authToken');
            await apolloClient.clearStore();
            dispatch({ type: LOGOUT, payload: { id: null } });
        } catch (e) {
            console.log(e)
        }
    }
};
